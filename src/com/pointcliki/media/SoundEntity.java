package com.pointcliki.media;

import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.SoundStore;
import com.pointcliki.core.Entity;

/**
 * SoundEntity adds a number of features to the basic Sound class, such as the
 * ability to play a sound from a specific point, and compatibility with the
 * snapshot mechanism of the PointCliki engine. A SoundEntity instance can also
 * be positioned in a scene and its volume varied depending on the distance the
 * viewport is from the source.
 * 
 * @author Hugheth
 * @since 3
 */
public class SoundEntity extends Entity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 3014550180338670391L;
	
	protected Audio fSound = null;
	
	/**
	 * Create a new sound entity
	 * @param name The name of the sound to load
	 */
	public SoundEntity(String name) {
		String file = "sounds/" + name + ".ogg";			
		SoundStore.get().init();
		try {
			if (file.toLowerCase().endsWith(".ogg")) {
				fSound = SoundStore.get().getOgg(file);
			} else if (file.toLowerCase().endsWith(".wav")) {
				fSound = SoundStore.get().getWAV(file);
			} else if (file.toLowerCase().endsWith(".aif")) {
				fSound = SoundStore.get().getAIF(file);
			} else if (file.toLowerCase().endsWith(".xm") || file.toLowerCase().endsWith(".mod")) {
				fSound = SoundStore.get().getMOD(file);
			} else {
				System.out.println("Can't load the sound file [" + file + "]");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * Play this sound effect at default volume and pitch
	 */
	public void play() {
		play(1.0f, 1.0f);
	}
	
	/**
	 * Play this sound effect at a given volume and pitch
	 * 
	 * @param pitch The pitch to play the sound effect at
	 * @param volume The volumen to play the sound effect at
	 */
	public void play(float pitch, float volume) {
		fSound.playAsSoundEffect(pitch, volume * SoundStore.get().getSoundVolume(), false);
	}
	
	/**
	 * Loop this sound effect at default volume and pitch
	 */
	public void loop() {
		loop(1.0f, 1.0f);
	}
	
	/**
	 * Loop this sound effect at a given volume and pitch
	 * 
	 * @param pitch The pitch to play the sound effect at
	 * @param volume The volumen to play the sound effect at
	 */
	public void loop(float pitch, float volume) {
		fSound.playAsSoundEffect(pitch, volume * SoundStore.get().getSoundVolume(), true);
	}
	
	
	/**
	 * Stop the sound being played
	 */
	public void stop() {
		fSound.stop();
	}
	
	public Audio audio() {
		return fSound;
	}
}
