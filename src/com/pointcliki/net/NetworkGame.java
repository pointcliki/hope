package com.pointcliki.net;

import com.pointcliki.core.Entity;
import com.pointcliki.core.PointClikiGame;

/**
 * @author Hugheth
 * @since 3
 */
public abstract class NetworkGame extends PointClikiGame {

	public NetworkGame(String title) {
		super(title);
	}
	protected PlayerManager<? extends Entity> fPlayerManager;
	protected NetManager fNetManager;
	
	/**
	 * Override this method to add your own managers to the game.
	 */
	public void configureManagers() {
		if (fManagers != null) return;
		
		super.configureManagers();
		
		fPlayerManager = new PlayerManager<Entity>(this);
		fNetManager = new NetManager(this);
	}
	
	public PlayerManager<?> getPlayerManager() {
		return fPlayerManager;
	}
	public NetManager getNetManager() {
		return fNetManager;
	}
}

