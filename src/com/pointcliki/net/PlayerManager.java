package com.pointcliki.net;

import java.util.HashMap;

import com.pointcliki.core.Entity;
import com.pointcliki.core.IManagerGroup;
import com.pointcliki.core.Manager;

public class PlayerManager<T extends Entity> extends Manager {

	/**
	 * Serial Key
	 */
	private static final long serialVersionUID = -5526326818087521729L;
	
	protected HashMap<String, Player<T>> fPlayers;
	
	public PlayerManager(IManagerGroup parent) {
		super(parent);
		fPlayers = new HashMap<String, Player<T>>();
	}
	
	public void add(Player<T> player) {
		fPlayers.put(player.name(), player);
		player.fParent = this;
	}

	@Override
	public void restore(Manager from) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}

	public Player<T> player(String name) {
		return fPlayers.get(name);
	}
}

