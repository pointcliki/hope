package com.pointcliki.net;

import com.pointcliki.core.Entity;
import com.pointcliki.core.EntityQuery;
import com.pointcliki.core.ISavable;

public abstract class Player<T extends Entity> implements ISavable {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 3315567580346092227L;
	
	protected PlayerManager<T> fParent;
	protected String fName;
	protected String fGruntColor;
	protected EntityQuery<T> fUnits;
	
	public Player(String name) {
		fName = name;
		fUnits = new EntityQuery<T>();
	}

	public void add(T unit) {
		fUnits.add(unit);
	}
	
	public void remove(T unit) {
		fUnits.remove(unit);
	}
	
	public String name() {
		return fName;
	}
	
	public PlayerManager<T> playerManager() {
		return fParent;
	}
}
