package com.pointcliki.text;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

import com.pointcliki.core.TextEntity;

/**
 * Counter text displays text of a fixed length like a counter
 * 
 * @author Hugheth
 * @since 3
 */
public class CounterText extends TextEntity {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -5645238782163340792L;
	
	protected int fWidth;
	protected char fDummy;
	protected String fDummyText;
	protected Color fSecondaryColor;
	protected int fDummyOffX;

	/**
	 * Create a new counter
	 * @param text The initial text
	 * @param width The number of columns to display
	 * @param dummy The dummy character for empty columns
	 * @param main The main font to use
	 * @param secondary The font of the dummy characters
	 */
	public CounterText(String text, int width, char dummy, UnicodeFont font) {
		super(text, TextEntity.ALIGN_RIGHT, font);
		fWidth = width;
		fDummy = dummy;
		calculateDummyString();
	}
	
	protected void calculateDummyString() {
		StringBuilder s = new StringBuilder();
		int i = fWidth - fText.length();
		while (i-- > 0) s.append(fDummy);
		fDummyText = s.toString();
		fDummyOffX = fFont.getWidth(fDummyText);
	}

	public void secondaryColor(Color color) {
		fSecondaryColor = color;
	}
	public void text(String string) {
		super.text(string);
		calculateDummyString();
	}
	
	@Override
	public void render(Graphics graphics, long currentTime) {
		super.render(graphics, currentTime);
		// Add alpha to secondary colour
		fSecondaryColor.a = fOpacity;
		fFont.drawString(-offX - fDummyOffX, -offY, fDummyText, fSecondaryColor);
	}
}
