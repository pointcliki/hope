package com.pointcliki.text;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.TextEntity;
import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.Minion;
import com.pointcliki.event.ProgressEvent;
import com.pointcliki.event.Rogue;
import com.pointcliki.transition.Lerper;
import com.pointcliki.transition.LinearLerper;
import com.pointcliki.transition.Transition;

public class FlashText extends TextEntity {
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 1368004624303338335L;

	protected Vector2f sTarget = new Vector2f(100, 250);
	
	protected Transition<FlashText> fTransition;
	protected Lerper fLerper;
	protected double fRotate;
	protected Vector2f fVector;
	
	/**
	 * Create text that flashes up on screen
	 * @param text The string to display
	 */
	public FlashText(String text, UnicodeFont font) {
		super(text, font);
		fTransition = new Transition<FlashText>(this, new Minion<ProgressEvent<FlashText>>(0) {

			@Override
			public long run(Dispatcher<ProgressEvent<FlashText>> dispatcher, String type, ProgressEvent<FlashText> event) {
				if (type == Rogue.ON_END || type == Rogue.ON_CANCEL) FlashText.this.cleanup();
				return 0;
			}
			
		}, 10, 0) {			
			/**
			 * Serial key
			 */
			private static final long serialVersionUID = 6607301224109866373L;

			@Override
			public void begin() {
				super.begin();
				// Randomly position
				position(position().add(new Vector2f((float) (Math.random() * 30 - 15), (float) (Math.random() * 30 - 15))));
			}

			@Override
			protected void run(Graphics graphics, long currentTime, float interval) {
				float m = interval * interval;
				if (interval < 0.2) {
					rotate(m * 360 * 26 + (float) fRotate);
					scale(new Vector2f(m * 20, m * 20));
					opacity(interval * 5);
				} else if (interval < 0.5) {
					// Do nothing
				} else {
					opacity((float) (1 - (interval - 0.5) * 2));
					position(position().sub(fVector.copy().scale(m - 0.25f)));
				}
			}
		};
		fLerper = new LinearLerper();
		opacity(0);
	}
	
	/**
	 * Show the text
	 */
	public void show() {
		if (fTransition.isRunning()) return;
		fTransition.setup((long) Math.floor(Math.random() * 500 + 1500), fLerper);
		fTransition.begin();
		fRotate = Math.random() * 50 - 25;
		fVector = position().sub(sTarget).scale(0.01f);
	}
}

