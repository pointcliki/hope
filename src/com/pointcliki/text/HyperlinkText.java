package com.pointcliki.text;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.geom.Vector2f;
import com.pointcliki.core.TextEntity;
import com.pointcliki.input.MouseEvent;

public abstract class HyperlinkText extends TextEntity {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 3764963642330502368L;
	private Color fNoHover;
	private Color fHover;
	
	public HyperlinkText(String stext, int align, UnicodeFont font, Color noHoverColor, Color hoverColor) {
		super(stext, align, font, noHoverColor);
		fHover = hoverColor;
		fNoHover = noHoverColor;
	}

	@Override
	public void render(Graphics graphics, long currentTime) {
		if (fHovering) fColor = fHover;
		else fColor = fNoHover;
		super.render(graphics, currentTime);
	}
	
	@Override
	public void handleUIMouseEvent(String type, Vector2f local, MouseEvent event) {
		if (type.equals("mouse.click")) click();
	}
	
	public abstract void click();
	
};
