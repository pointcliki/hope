package com.pointcliki.text;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;

import com.pointcliki.core.TextEntity;

public class BoxedText extends TextEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 1989069231553450894L;

	private Color fBack;
	private int fWidth;
	private int fHeight;
	private int fPadding;
	private int fRounded;
	
	public BoxedText(String stext, int align, UnicodeFont font, Color color, Color back, int padding, int rounded) {
		super(stext, align, font, color);
		fBack = back;
		fPadding = padding;
		fRounded = rounded;
		calculateSize();
	}
	
	protected void calculateSize() {
		fWidth = fFont.getWidth(fText) + fPadding * 2;
		fHeight = fFont.getHeight(fText) + fPadding * 2;
	}
	
	@Override
	public void text(String string) {
		super.text(string);
		calculateSize();
	}
	
	@Override
	public void render(Graphics graphics, long currentTime) {		
		graphics.setColor(fBack);
		graphics.fillRoundRect(-fPadding, -(fHeight / 2), fWidth, fHeight, fRounded);
		
		super.render(graphics, currentTime);
	}
}
