package com.pointcliki.layered;

import com.pointcliki.map.LayeredMap;
import com.pointcliki.map.MapEntity;

/**
 * A layered map entity draws a map consisting of a number of layers
 * 
 * @author Hugheth
 * @since 3
 */
public class LayeredMapEntity extends MapEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 1899684819622855298L;

	public LayeredMapEntity(LayeredMap m) {
		super(m);
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}
}
