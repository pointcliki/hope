package com.pointcliki.random;

/**
 * A random variable is an object that allows a number to be picked between a
 * range with a particular probability distribution. This implementation uses
 * a float.
 * 
 * @author Hugheth
 */
public class RandomVariable {
	
	protected float fMin;
	protected float fMax;
	protected float fRange;
	
	/**
	 * Create a new random variable
	 * @param min The lowest possible value
	 * @param max The highest possible value
	 */
	public RandomVariable(float min, float max) {
		fMin = min;
		fMax = max;
		fRange = fMax - fMin;
	}
	
	/**
	 * Create a new random variable
	 * @param constant A constant value
	 */
	public RandomVariable(float constant) {
		this(constant, constant);
	}
	
	/**
	 * The default random variable returns a value in the range with a
	 * linear distribution.
	 * 
	 * @return A random value in the min / max range
	 */
	public float value() {
		if (fRange == 0) return fMin;
		return (float) (Math.random() * fRange + fMin);
	}
}