package com.pointcliki.random;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * The random order class contains a handful of helpful
 * functionalities when producing randomised gameplay.
 * 
 * @author Hugheth
 * @since 3
 */
public class RandomOrder<T extends Object> {

	protected ArrayList<T> fList = new ArrayList<T>();
	protected boolean shuffled = false;
	
	public RandomOrder() {
		
	}
	
	public void add(T item) {
		shuffled = false;
		fList.add(item);
	}
	
	public void addMany(T item, int frequency) {
		while (frequency-- > 0) add(item);
	}
	
	public void addAll(T[] array) {
		for (T item: array) add(item);
	}
	
	public void addAll(Collection<? extends T> items) {
		shuffled = false;
		fList.addAll(items);
	}
	
	/**
	 * Returns the head of the order, without consuming it. For example, you
	 * can use this method to check whether you want to consume the head, or
	 * shuffle and consume another item
	 * @return
	 */
	public T peek() {
		if (fList.isEmpty()) return null;
		return fList.get(fList.size() - 1);
	}
	
	/**
	 * This removes the top of the list and places it in a random position
	 * somewhere else in the order 
	 */
	public void shuffle() {
		T last = fList.get(fList.size() - 1);
		int mid = (int) Math.floor(Math.random() * (fList.size() - 1));
		// Swap with another
		fList.set(fList.size() - 1, fList.get(mid));
		fList.set(mid, last);
	}
	
	/**
	 * This consumes a single element from the order, returning the element
	 * and removing it from the 
	 * @return The consumed element
	 */
	public T consume() {
		if (fList.isEmpty()) return null;
		if (!shuffled) {
			Collections.shuffle(fList);
			shuffled = true;
		} 
		return fList.remove(fList.size() - 1);
	}
	
	public int size() {
		return fList.size();
	}
}
