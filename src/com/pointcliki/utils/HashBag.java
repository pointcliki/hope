package com.pointcliki.utils;

import java.util.ArrayList;
import java.util.HashMap;

public class HashBag<A, B> {

	private HashMap<A, ArrayList<B>> fMap;

	public HashBag() {
			
		fMap = new HashMap<A, ArrayList<B>>();
		
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<B> get(Object key) {
		if (!fMap.containsKey(key)) fMap.put((A) key, new ArrayList<B>());
		return fMap.get(key);
	}
	
	public B get(A key, int i) {
		return get(key).get(i);
	}
	
	public B set(A key, B value, int i) {
		get(key).set(i, value);
		return value;
	}
	
	public B add(A key, B value) {
		get(key).add(value);
		return value;
	}
	
	public B remove(A key, B value) {
		get(key).remove(value);
		return value;
	}
}
