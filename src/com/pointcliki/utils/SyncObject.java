package com.pointcliki.utils;

public class SyncObject<T> {

	private T fValue;
	
	public SyncObject(T val) {
		fValue = val;
	}
	
	public synchronized void set(T val) {
		fValue = val;
	}
	public synchronized T get() {
		return fValue;
	}
}
