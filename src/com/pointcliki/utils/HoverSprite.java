package com.pointcliki.utils;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.PointClikiGame;
import com.pointcliki.input.MouseEvent;
import com.pointcliki.ui.UIEntity;

public abstract class HoverSprite extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 6350500322718625797L;
	
	protected Image fImage;
	protected Image fHoverImage;
	
	public HoverSprite(String string, String string2) {
		this(PointClikiGame.resourceManager().image(string), PointClikiGame.resourceManager().image(string2));
	}
	
	public HoverSprite(Image img, Image img2) {
		fImage = img;
		fHoverImage = img2;
		resize(new Vector2f(img.getWidth(), img.getHeight()));
	}
	
	@Override
	public void render(Graphics graphics, long currentTime) {
		super.render(graphics, currentTime);
		Image img;
		if (!fHovering)
			img = fImage;
		else
			img = fHoverImage;
		img.setAlpha(opacity());
		img.draw(0, 0);
		img.setAlpha(1);	// Restore in case the image is used elsewhere
	}
	
	@Override
	public void handleUIMouseEvent(String type, Vector2f local, MouseEvent event) {
		if (type.equals("mouse.click")) click();
	}
	
	public abstract void click();
}
