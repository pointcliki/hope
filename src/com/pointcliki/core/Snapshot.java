package com.pointcliki.core;

/**
 * A snapshot is used to save an restore game state, useful when
 * saving levels, passing information across the internet and dealing with
 * network lag 
 * 
 * @author Hugheth
 * @since 3
 */
public class Snapshot implements ISavable {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 190949328479539458L;

	/**
	 * Take a snapshot of this snapshot, allowing snapshots to be duplicated
	 */
	@Override
	public ISavable snapshot() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}
	
}
