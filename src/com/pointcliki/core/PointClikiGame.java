package com.pointcliki.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

import com.pointcliki.input.InputManager;
import com.pointcliki.io.ResourceManager;

/**
 * The Game class is a good starting point for any game.
 * It can contain any number of scenes. Override this class when making your
 * own game to add scenes when the game begins.
 *  
 * @author hugheth
 * @since 1
 * @see Scene
 * @see Manager
 */
public abstract class PointClikiGame extends BasicGame implements IManagerGroup {

	protected static AppGameContainer sApp;
	protected static PointClikiGame sGame;
	protected List<Scene> fScenes;
	protected Scene fCurrentScene;
	protected Rectangle fBounds;
	
	protected Map<Class<? extends Manager>, Manager> fManagers = null;
	protected ResourceManager fResourceManager;
	protected InputManager fInputManager;
	protected EntityManager fEntityManager;

	/**
	 * Create a new game
	 * @param title The title of the game
	 */
	public PointClikiGame(String title) {
		super(title);
		fScenes = new ArrayList<Scene>();
	}
	
	@Override
	public void init(GameContainer c) throws SlickException {
		// Get the bounds
		fBounds = new Rectangle(0, 0, sApp.getWidth(), sApp.getHeight());

		// Configure the game managers
		configureManagers();
		// Configure the scenes
		configureScenes();
	}

	@Override
	public void render(GameContainer c, Graphics graphics) throws SlickException {
		fCurrentScene.render(graphics, -1);
	}

	@Override
	public void update(GameContainer c, int delta) throws SlickException {
		for (Scene s: fScenes) {
			s.update(c, delta);
		}
	}
	
	@Override
	public void keyPressed(int key, char c) {
		fInputManager.addKey(key, c);
	}
	@Override
	public void keyReleased(int key, char c) {
		fInputManager.removeKey(key, c);
	}
	@Override
	public void mousePressed(int button, int x, int y) {
		fInputManager.mousePressed(button, x, y);
	}
	@Override
	public void mouseReleased(int button, int x, int y) {
		fInputManager.mouseReleased(button, x, y);
	}
	@Override
	public void mouseClicked(int button, int x, int y, int count) {
		fInputManager.mouseClicked(button, x, y, count);
	}
	@Override
	public void mouseMoved(int ox, int oy, int x, int y) {
		fInputManager.mouseMoved(ox, oy, x, y);
	}
	@Override
	public void mouseDragged(int ox, int oy, int x, int y) {
		fInputManager.mouseDragged(ox, oy, x, y);
	}
	
	/**
	 * @return A list of scenes
	 */
	public List<Scene> scenes() {
		return fScenes;
	}
	/**
	 * @param id The id of the scene
	 * @return The scene with the id passed
	 */
	public static Scene scene(long id) {
		for (Scene s: sGame.fScenes) if (s.fID == id) return s;
		return null;
	}
	
	public void setCurrentScene(Scene s) {
		fCurrentScene = s;
	}

	/**
	 * @return The instance of the application currently running the game
	 */
	public AppGameContainer application() {
		return sApp;
	}
	/**
	 * @return The instance of the game currently running
	 */
	public static PointClikiGame instance() {
		return sGame;
	}
	
	/**
	 * Override this method if you want to set up the application differently.
	 */
	public static void play(PointClikiGame game) {
		sGame = game;
		try {
			sApp = new AppGameContainer(sGame);
			sApp.setDisplayMode(960, 720, false);
			sApp.setVerbose(false);
			sApp.setShowFPS(false);
			sApp.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Override this method to add your own managers to the game.
	 */
	@Override
	public void configureManagers() {
		if (fManagers != null) return;
		
		// Create the managers
		fResourceManager = new ResourceManager("lib");
		fInputManager = new InputManager();
		fEntityManager = new EntityManager();
		
		// Add to the manager set
		fManagers = new HashMap<Class<? extends Manager>, Manager>();
		fManagers.put(ResourceManager.class, fResourceManager);
		fManagers.put(InputManager.class, fInputManager);
		fManagers.put(EntityManager.class, fEntityManager);
	}

	/**
	 * Override this method to add your own scenes to the game.
	 */
	protected abstract void configureScenes();
	
	/**
	 * @return The resource manager of the game
	 */
	public static ResourceManager resourceManager() {
		return sGame.fResourceManager;
	}
	/**
	 * @return The input manager of the game
	 */
	public static InputManager inputManager() {
		return sGame.fInputManager;
	}
	/**
	 * @return The entity manager of the game
	 */
	public static EntityManager entityManager() {
		return sGame.fEntityManager;
	}

	@Override
	public List<Manager> managers() {
		return new ArrayList<Manager>(fManagers.values());
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T manager(Class<T> type) {
		if (!fManagers.containsKey(type)) return null;
		return (T) fManagers.get(type);
	}

	@Override
	public IManagerGroup parentManagerGroup() {
		return null;
	}
}

