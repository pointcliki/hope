package com.pointcliki.core;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

public class Sprite extends Entity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 4039185336461096578L;
	
	protected Image fImage;
	protected boolean fIsSpriteSheet;
	protected int fSubX;
	protected int fSubY;
	
	public Sprite() {}

	public Sprite(Image image) {
		image(image);
	}
	public Sprite(SpriteSheet sheet) {
		fIsSpriteSheet = true;
		image(sheet);
	}
	public Sprite(String string) {
		image(PointClikiGame.resourceManager().image(string));
	}
	public Sprite(String string, int width, int height) {
		fIsSpriteSheet = true;
		image(PointClikiGame.resourceManager().spritesheet(string, width, height));
	}

	@Override
	public ISavable snapshot() throws CloneNotSupportedException {
		return (ISavable) super.clone();
	}

	@Override
	public void cleanup() {
		/*
		 * The image should be shared by the ResourceManager, so
		 * we shouldn't destroy its internals here
		 */ 
		fImage = null;
	}
	
	public Sprite image(Image image) {
		fImage = image;
		if (fImage != null) fSpan = new Rectangle(0, 0, image.getWidth(), image.getHeight());
		else fSpan = new Rectangle(0, 0, 0, 0);
		return this;
	}
	public Sprite image(String string) {
		image(PointClikiGame.resourceManager().image(string));
		return this;
	}
	public Sprite subimage(int x, int y) {
		fSubX = x;
		fSubY = y;
		return this;
	}

	@Override
	public void render(Graphics graphics, long currentTime) {
		if (fImage == null) return;
		super.render(graphics, currentTime);
		Image img;
		if (fIsSpriteSheet) img = ((SpriteSheet) fImage).getSubImage(fSubX, fSubY);
		else img = fImage;
		img.setAlpha(opacity());
		img.draw(0, 0);
		img.setAlpha(1);	// Restore in case the image is used elsewhere
		
		//fImage.draw(fBounds.getX(), fBounds.getY(), fBounds.getX(), fBounds.getY(), fBounds.getWidth(), fBounds.getHeight());
	}
	
	@Override
	public String toString() {
		if (fImage == null) return "[Sprite]";
		return "[Sprite: " + fImage.getName() + "]";
	}
	public Image image() {
		return fImage;
	}
}
