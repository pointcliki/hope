package com.pointcliki.core;

import java.util.Iterator;
import java.util.TreeSet;

/**
 * The EntityQuery class is used to keep track of a set of entities, and can
 * be extended to allow customised groups of entities to be selected depending
 * on their properties. Most importantly, the entity query class maintains
 * references to the correct entities when snapshots of the game are taken,
 * such as during a game save or a network lag.
 * 
 * The EntityQuery class should always be used when you need to keep pointers
 * to one or more entities.
 * 
 * @author Hugheth
 * @since 3
 *
 * @param <T> The type of entity that the query contains
 * 
 * @see EntityManager
 */
public class EntityQuery<T extends Entity> implements ISavable, Iterable<T> {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 902475774954919313L;
	
	/**
	 * The list of IDs of entities being stored in the query
	 */
	protected TreeSet<Long> fIDs;
	/**
	 * The cache of entities so retrieval can be quicker
	 */
	transient protected TreeSet<T> fCache;
	/**
	 * Whether the cache of the query is stale and should be refreshed
	 */
	protected boolean fStale = false;

	/**
	 * Create a new query for a singleton entity
	 * @param entity A singleton entity to be tracked by this query
	 */
	public EntityQuery(T entity) {
		fIDs = new TreeSet<Long>();
		fCache = new TreeSet<T>();
		add(entity);
	}
	
	/**
	 * Create a new empty entity query
	 */
	public EntityQuery() {
		fIDs = new TreeSet<Long>();
		fCache = new TreeSet<T>();
	}

	/**
	 * Get the first entity managed by this query, or its only entity of the
	 * query is being used to track a single entity
	 * @return The first entity of the query
	 */
	public T entity() {
		if (fStale) refresh();
		return fCache.first();
	}
	
	/**
	 * Add a new entity to the query
	 * @param entity The new entity to track
	 * @return The entity query instance
	 */
	public T add(T entity) {
		if (fIDs.contains(entity.id())) return entity;
		fIDs.add(entity.id());
		fCache.add(entity);
		return entity;
	}
	
	/**
	 * Remove a new entity from the query
	 * @param entity The entity to remove
	 * @return The entity query instance
	 */
	public EntityQuery<T> remove(T entity) {
		fIDs.remove(entity.id());
		Iterator<T> it = fCache.iterator();
		while (it.hasNext()) if (it.next().id() == entity.id()) {
			it.remove();
			break;
		}
		return this;
	}
	
	/**
	 * Update an entity if its order has changed
	 * @param entity
	 * @return
	 */
	public EntityQuery<T> update(T entity) {
		remove(entity);
		add(entity);
		return this;
	}
		
	/**
	 * Refresh retrieves entities from the entity manager to populate the cache.
	 * @return The entity query instance
	 */
	@SuppressWarnings("unchecked")
	public EntityQuery<T> refresh() {
		fCache = new TreeSet<T>();
		for (long id: fIDs) fCache.add((T) PointClikiGame.entityManager().entity(id));
		fStale = false;
		return this;
	}
	
	/**
	 * Empty the query of all entities
	 * @return The entity query instance
	 */
	public EntityQuery<T> empty() {
		fIDs.clear();
		fCache.clear();
		fStale = false;
		return this;
	}

	@Override
	public ISavable snapshot() throws CloneNotSupportedException {
		return clone();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public EntityQuery<T> clone() throws CloneNotSupportedException {
		return (EntityQuery<T>) super.clone();
	}
	/**
	 * Returns an iterator which can be used to iterate through the entities
	 * of the query. A clone of the entity list is used, meaning that the
	 * query object can have its contents changed during the iteration.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Iterator<T> iterator() {
		if (fStale) refresh();
		TreeSet<T> cache = (TreeSet<T>) fCache.clone();
		return cache.iterator();
	}
	/**
	 * Returns an iterator which traverses the entities in the
	 * reverse order that they are stored.
	 */
	@SuppressWarnings("unchecked")
	public Iterator<T> descendingIterator() {
		if (fStale) refresh();
		TreeSet<T> cache = (TreeSet<T>) fCache.clone();
		return cache.descendingIterator();
	}
	
	/**
	 * Clean up the entity query when it is no longer required
	 */
	public void cleanup() {
		fIDs = null;
		fCache = null;
	}
	
	/**
	 * @return The size of the query
	 */
	public int size() {
		if (fIDs == null) return 0;
		return fIDs.size();
	}
}
