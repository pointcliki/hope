package com.pointcliki.core;

import java.util.Iterator;

import org.newdawn.slick.Graphics;

/**
 * An entity container groups a number of entities together such that
 * transformations on the entity container affect all its children. Entities
 * parented to an entity container are rendered atomically, in an order
 * determined by the entity's order field.
 * 
 * An entity can only belong to a single container at any one time.
 * 
 * @author Hugheth
 * @since 1
 */
public class EntityContainer extends Entity implements ISavable, Iterable<Entity> {

	/**
	 * Serial key 
	 */
	private static final long serialVersionUID = -6514711010991066274L;
		
	protected EntityQuery<Entity> fChildren = new EntityQuery<Entity>();

	/**
	 * Add an entity to this group
	 * @param entity The entity to add to this group
	 */
	public Entity addChild(Entity entity) {
		// Remove from old parent
		if (entity.getParent() != null) entity.getParent().removeChild(entity);
		// Update the entity's parent
		entity.setParent(this);
		// Add to set
		fChildren.add(entity);
		return entity;
	}
	
	/**
	 * @param entity The entity to add to the container
	 * @param order The ordering value of the entity
	 */
	public Entity addChild(Entity entity, int order) {
		entity.fOrder = order;
		addChild(entity);
		return entity;
	}

	/**
	 * @param entity The entity to be removed from the container
	 */
	public void removeChild(Entity entity) {
		if (entity.fParent != this) return;
		// Remove the entity from the set
		fChildren.remove(entity);
		// Update the parent of the entity
		entity.setParent(null);
	}
	
	/**
	 * @return The number of child entities, or 0 if the container has been cleaned up
	 */
	public int childCount() {
		if (fChildren == null) return 0;
		return fChildren.size();
	}
	
	/**
	 * Update the scene of the entity container
	 * @param Scene The new scene of the container
	 */
	@Override
	protected void setScene(Scene scene) {
		super.setScene(scene);
		for (Entity entity: this) entity.setScene(scene);
	}
	
	/*
	 * When a snapshot is restored, we need to re-associate entities in the
	 * scene with the scene and their parents, as we loose this information
	 * during a clone as we can't get references to the cloned parent and
	 * scene  
	 * @param parent
	 /
	public void refreshDescendants(Entity parent) {
		// Wipe our cache
		fCache.clear();
		// Iterate through children and update
		for (Entity e: holdChildren()) {
			e.setParent(this);
			if (e instanceof EntityContainer) ((EntityContainer) e).refreshDescendants(this);
		}
	}
	*/
	
	@Override
	public void render(Graphics graphics, long currentTime) {
		super.render(graphics, currentTime);
		renderEntities(graphics, currentTime);
	}
	
	protected void renderEntities(Graphics graphics, long currentTime) {
		// Render the entitys
		for (Entity entity: this) {
			// Cache position in case it moves
			int x = (int) entity.fPosition.getX();
			int y = (int) entity.fPosition.getY();
			int ox = (int) entity.origin().x;
			int oy = (int) entity.origin().y;
			float sx = entity.fScale.x;
			float sy = entity.fScale.y;
			float rot = entity.fRotation;
			// Transform the graphics
			graphics.translate(x + ox, y + ox);
			graphics.rotate(0, 0, -rot);
			graphics.scale(sx, sy);
			graphics.translate(-ox, -oy);
			try {
				entity.render(graphics, currentTime);
			} finally {
				// Transform the graphics back
				graphics.translate(ox, oy);
				graphics.scale(1 / sx, 1 / sy);
				graphics.rotate(0, 0, rot);
				graphics.translate(-ox, -oy);
				graphics.translate(-ox-x, -oy-y);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public ISavable snapshot() throws CloneNotSupportedException {
		// Clone this object
		EntityContainer clone = (EntityContainer) super.snapshot();
		// Clone the children set
		clone.fChildren = (EntityQuery<Entity>) fChildren.snapshot();
		return clone;		
	}

	@Override
	public void cleanup() {
		for (Entity e: this) e.cleanup();
		super.cleanup();
		fChildren.cleanup();
		fChildren = null;
	}
	
	@Override
	public Entity opacity(float a) {
		for (Entity e: fChildren) e.opacity(a);
		return super.opacity(a);
	}
	
	@Override
	public String toString() {
		String s = "[Container #" + fID + ":";
		if (fChildren == null) return s + " DEAD]";
		for (Entity entity: this) s += " " + entity.toString();
		s += "]";
		return s;
	}

	@Override
	public Iterator<Entity> iterator() {
		assert fChildren != null:
			"Can't iterate through the container as it has been cleaned up!";
		return fChildren.iterator();
	}
}

