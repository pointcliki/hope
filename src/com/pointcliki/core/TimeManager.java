package com.pointcliki.core;

import org.newdawn.slick.GameContainer;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.FrameManager;
import com.pointcliki.event.IEvent;

/**
 * The time manager keeps track of time in a scene, and is vital as it
 * determines the rate of logical frames to give the player a smooth experience
 * while coping with network lag 
 * 
 * @author Hugheth
 * @since 1
 */
public class TimeManager extends Manager {
	
	protected TimeManagerState s = new TimeManagerState();
	protected TimeDispatcher fDispatcher = new TimeDispatcher();
	
	transient protected boolean fPaused = true;
	transient protected boolean fStopped = true;

	/**
	 * Serial Key
	 */
	private static final long serialVersionUID = 1738455447538855777L;

	/**
	 * Create a new time manager
	 * @param scene The scene which the time manager controls
	 * @param frameRate The frame rate of the scene
	 */
	public TimeManager(Scene scene, int frameRate) {
		super(scene);
		s.frameRate = frameRate;
		s.frameDuration = 1000.0f / frameRate;
	}
	
	/**
	 * Start the scene
	 * @param currentTime The time at which the scene is starting
	 */
	public void start(long currentTime) {
		fPaused = false;
		s.startTime = currentTime;
		s.fZeroTime += currentTime - s.fPauseTime;
	}
	
	/**
	 * Stop the scene
	 */
	public void stop() {
		fPaused = true;
		fStopped = true;
		s.fPauseTime = s.currentTime;
	}
	
	/**
	 * Resume the scene
	 */
	public void resume() {
		start(s.fPauseTime);
	}
	
	/**
	 * Update the time manager to check for a new frame
	 * @param c The game container
	 * @param delta The amount of time that has passed since the last time this method was called
	 */
	public void update(GameContainer c, int delta) {

		if (fPaused) return;

		// Set the current time
		s.currentTime = c.getTime() - s.fZeroTime;
		s.realTime = c.getTime() - s.startTime;
		
		fDispatcher.dispatchEvent(delta);
		
		// Check for logical frame
		while (s.currentTime - s.lastFrameTime >= s.frameDuration) {
			//if (again) System.out.print(".");
			s.lastFrameTime += s.frameDuration;
			// Increment current frame
			s.currentFrame++;
			// Update the event manager
			fParent.manager(FrameManager.class).tick(s.currentFrame);
			
		}
	}
	
	public Dispatcher<TimeEvent> dispatcher() {
		return fDispatcher;
	}
	
	public int frameRate() {
		return s.frameRate;
	}
	public float frameDuration() {
		return s.frameDuration;
	}
	
	public long currentFrame() {
		return s.currentFrame;
	}
	public long currentTime() {
		return s.currentTime;
	}
	public long realTime() {
		return s.realTime;
	}
	public boolean isPaused() {
		return fPaused;
	}

	@Override
	public void restore(Manager from) {
		TimeManager copy = (TimeManager) from;
		s = copy.s;
	}
	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		TimeManager clone = (TimeManager) super.clone();
		clone.s = s.clone();
		return clone;
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Start time for the scene
	 */
	public void start() {
		start(PointClikiGame.instance().application().getTime());
	}
	
	protected class TimeManagerState implements Cloneable {
		public int frameRate;
		public float frameDuration;
		

		public long realTime;
		public long startTime;
		
		public long currentFrame = 0;
		public long currentTime = 0;
		public long lastFrameTime = 0;
		
		/**
		 * The zero time records the start time plus the offset in time
		 * due to the game being paused, thus acts as a measure of duration
		 * when current time is subtracted from it
		 */
		public long fZeroTime = 0;
		public long fPauseTime = 0;
		
		@Override
		public TimeManagerState clone() throws CloneNotSupportedException {
			return (TimeManagerState) super.clone();
		}
	}
	
	/**
	 * Just use a constant 
	 */
	@Override
	public int hashCode() {
		return (int) (serialVersionUID % Integer.MAX_VALUE);
	}

	/**
	 * The number of frames elapsed after the time period specified has elapsed. 
	 * @param timeDuration The time duration to calculate frames for
	 * @return The number of frames elapsed over the time duration
	 */
	public float frameDuration(long timeDuration) {
		return timeDuration / s.frameDuration;
	}
	
	/**
	 * @param frames The number of frames from now
	 * @return What frame it will be then
	 */
	public long frameIn(long frames) {
		return s.currentFrame + frames;
	}
	
	/**
	 * Allow classes to register interest in Slick updates
	 * @author Hugheth
	 * @since 3
	 */
	public static class TimeEvent implements IEvent {
		
		protected int fDelta;
		public static final String TYPE = "update";
		
		protected TimeEvent(int delta) {
			fDelta = delta;
		}
		public int delta() {
			return fDelta;
		}
	}
	
	public static class TimeDispatcher extends Dispatcher<TimeEvent> {
		/**
		 * Serial key
		 */
		private static final long serialVersionUID = -9164261608968289077L;

		public void dispatchEvent(int delta) {
			dispatchEvent(TimeEvent.TYPE, new TimeEvent(delta));
		}
	}
}

