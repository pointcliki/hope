package com.pointcliki.core;

/**
 * A manager is a special class of entity that is initiated as a singleton
 * instance of a scene. Managers should not be destroyed, and should be
 * assigned its scene when it is constructed. 
 *  
 * @author hugheth
 * @since 1
 * 
 * @see IManagerGroup
 * @see Scene
 * @see PointClikiGame
 */
public abstract class Manager implements ISavable {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -3396052165485645305L;
	
	protected IManagerGroup fParent;
	
	/**
	 * Create a new manager
	 * @param parent The parent of the manager
	 */
	public Manager (IManagerGroup parent) {
		fParent = parent;
	}
	
	/**
	 * Return a manager object that can be serialised to represent this class.
	 * This may not be an exact clone of the instance, if the manager needs to
	 * preserve state across saves
	 * 
	 * Note that managers attached to the Game will never use this method, as
	 * they are transient
	 * 
	 * @return The manager that represents this instance in its current state
	 */
	public abstract Manager snapshot() throws CloneNotSupportedException;

	/**
	 * Unlike other entities, managers are specifically restored when a snapshot
	 * is restored. This allows a manager to compare its current version with
	 * the version that should be restored, making the necessary updates to the
	 * manager
	 * @see SnapshotManager
	 */
	public abstract void restore(Manager from);
	
	/**
	 * Clean up any resources that the manager was using prior to it being
	 * destroyed.
	 */
	public abstract void cleanup();
	
	/**
	 * Get the parent scene of the manager
	 */
	public IManagerGroup parent() {
		return fParent;
	}
}

