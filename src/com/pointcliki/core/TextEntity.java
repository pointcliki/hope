package com.pointcliki.core;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.geom.Rectangle;

import com.pointcliki.ui.UIEntity;

public class TextEntity extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 5027650155940350578L;
		
	protected String fText;
	protected Color fColor = Color.white.scaleCopy(1);
	protected int offX;
	protected int offY;
	protected UnicodeFont fFont;
	protected int fAlign;
	
	public static final int ALIGN_LEFT = 0;
	public static final int ALIGN_CENTER = 1;
	public static final int ALIGN_RIGHT = 2;
		
	public TextEntity(String text, UnicodeFont font) {
		this(text, ALIGN_CENTER, font);
	}
	
	public TextEntity(String stext, int align, UnicodeFont font) {
		fFont = font;
		fAlign = align;
		text(stext);
	}
	
	public TextEntity(String stext, int align, UnicodeFont font, Color color) {
		this(stext, align, font);
		fColor = color;
	}
	
	@Override
	public void render(Graphics graphics, long currentTime) {
		super.render(graphics, currentTime);
		// Add alpha to colour
		fColor.a = fOpacity;
		fFont.drawString(-offX, -offY, fText, fColor);
	}

	@Override
	public ISavable snapshot() throws CloneNotSupportedException {
		return null;
	}

	public void text(String string) {
		fText = string;
		if (fAlign == ALIGN_LEFT) offX = 0;
		else if (fAlign == ALIGN_CENTER) offX = fFont.getWidth(string) / 2;
		else if (fAlign == ALIGN_RIGHT) offX = fFont.getWidth(string);
		offY = fFont.getHeight(string) / 2;
		fSpan = new Rectangle(-offX, -offY, fFont.getWidth(string), fFont.getHeight(string));
	}
	public String text() {
		return fText;
	}
	
	public void color(Color color) {
		fColor = color;
	}
	
	@Override
	public String toString() {
		return "[TextEntity \"" + fText + "\"]";
	}
}
