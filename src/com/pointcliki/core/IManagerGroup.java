package com.pointcliki.core;

import java.util.List;

/**
 * The IManagerGroup interface allows managers to access other managers in the
 * same manager group 
 * 
 * @author hugheth
 * @since 3
 * @see Manager
 */
public interface IManagerGroup {
	
	/**
	 * Initialise the managers for the first time
	 */
	public void configureManagers();
	
	/**
	 * Get a list of the managers in this group
	 * @return Manager list
	 */
	public List<Manager> managers();
	
	/**
	 * Get a manager that is included in this group. If a manager is not found,
	 * a recursive call is made to the parent of the group.
	 * @param type The class of the manager instance to return
	 * @return The manager instance found, or null otherwise 
	 */
	public <T> T manager(Class<T> type);
	
	/**
	 * @return The manager group that this group is parented to, null otherwise 
	 */
	public IManagerGroup parentManagerGroup();
}
