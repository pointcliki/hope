package com.pointcliki.core;

import java.io.Serializable;

/**
 * The ISavable interface is designed to mark classes that
 * can be saved during the running of a game. All classes
 * in the game hierarchy should implement at least one of
 * ISavable or ITransient to indicate whether or not they
 * should or shouldn't be saved. A common rule is that
 * any classes that hold some game state should be ISavable,
 * and any that should persist during loading of a new game
 * state (e.g. a load of a level or a recovery from network
 * lag) should be ITransient.
 * 
 * @author Hugheth
 * @since 3
 * @see IStateless
 */
public interface ISavable extends Serializable, Cloneable {

	/**
	 * Return a savable object that can be serialised to represent this class.
	 * Typically, this will be an exact clone of the instance. A clone should
	 * be a deep enough clone to ensure that 
	 * 
	 * @return The entity that represents this instance in its current state
	 * @throws CloneNotSupportedException If the clone can't happen
	 */
	public abstract ISavable snapshot() throws CloneNotSupportedException;
	
}
