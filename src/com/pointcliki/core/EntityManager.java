package com.pointcliki.core;

import java.util.HashMap;

/**
 * The EntityManager is a global tracker of Entities that allows classes to
 * always maintain a way of accessing the correct versions of entities in the
 * game. Because entities are cloned when they are saved, it may be difficult
 * to ensure that references to entities are kept up to date, so they should
 * instead use an EntityQuery which automatically retrieves the latest entity
 * if an older or different game state is loaded.
 * 
 * An entity has an ID which is unique to that entity for the duration it is
 * attached to a scene. When an entity changes scene, it is re-registered so
 * that the entity can be restored in its previous scene if that scene loads
 * an older game state where the entity was still parented to that scene.
 * 
 * @see EntityQuery
 * 
 * @author hugheth
 * @since 1
 */
public class EntityManager extends Manager {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -5043102333394917333L;
	
	protected static long uid = 0;
	protected HashMap<Long, Entity> fEntities = new HashMap<Long, Entity>();

	public EntityManager() {
		super(null);
	}
	
	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		EntityManager clone = (EntityManager) super.clone();
		// Need to iterate through entities to clone them
		clone.fEntities = new HashMap<Long, Entity>();
		for (long id: fEntities.keySet()) clone.fEntities.put(id, (Entity) fEntities.get(id).snapshot());
		return clone;
	}

	@Override
	public void restore(Manager from) {
		EntityManager copy = (EntityManager) from;
		fEntities = copy.fEntities;
	}

	@Override
	public void cleanup() {
		fEntities = null;
	}

	/**
	 * @param entity The entity to register with the entity manager
	 */
	public void register(Entity entity) {
		// Remove if it was already registered
		if (entity.fID >= 0) fEntities.remove(entity.fID);
		// Choose a new ID
		entity.fID = uid++;
		restore(entity);		
	}
	
	/**
	 * @param entity Unregister the entity from the entity manager
	 */
	public void unregister(Entity entity) {
		fEntities.remove(entity.fID);
		entity.fID = -1;
	}
	
	/**
	 * This overwrites a previous entity that had the same id
	 * @param entity The entity to restore to the entity manager
	 */
	public void restore(Entity entity) {
		fEntities.put(entity.fID, entity);
	}

	/**
	 * @param id The id of the entity to retrieve
	 * @return The entity of id asked
	 */
	public Entity entity(long id) {
		assert fEntities.containsKey(id): "[Entity " + id + "] doesn't exist!";
		return fEntities.get(id);
	}
	
	/**
	 * @param id The id of the entity to retrieve
	 * @param cls The type of the entity to return
	 * @return The entity with the id and of type asked, null if it doesn't exist
	 */
	@SuppressWarnings("unchecked")
	public <V> V entity(long id, Class<V> cls) {
		Entity e = entity(id);
		assert e.getClass().equals(cls): "[Entity " + id + "] not of type: " + cls;
		return (V) e;
	}
	
	/**
	 * Just use a constant 
	 */
	@Override
	public int hashCode() {
		return (int) (serialVersionUID % Integer.MAX_VALUE);
	}
}
