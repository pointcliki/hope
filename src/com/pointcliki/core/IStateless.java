package com.pointcliki.core;

/**
 * The IStateless interface is a marker interface to indicate that the class
 * contains no game state or related information.
 * 
 * Classes which implement IStateless should not implement ISavable.
 * 
 * @author Hugheth
 * @since 3
 */
public interface IStateless {
	// No content required for marker interface
}
