package com.pointcliki.particles;

import com.pointcliki.core.Entity;
import com.pointcliki.core.PointClikiGame;
import com.pointcliki.core.ISavable;
import com.pointcliki.core.Scene;
import com.pointcliki.core.TimeManager.TimeEvent;
import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.Minion;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.particles.ParticleEmitter;
import org.newdawn.slick.particles.ParticleSystem;

/**
 * @author Hugheth
 * @since 3
 */
public class ParticleLayer extends Entity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 4039185336461096578L;
	
	protected ParticleSystem fParticles;
	protected Minion<TimeEvent> fUpdater;
	
	public ParticleLayer() {
		this(null);
	}
	
	public ParticleLayer(String img) {
		fParticles = new ParticleSystem(PointClikiGame.resourceManager().image(img));
		fUpdater = new Minion<TimeEvent>(0) {
			@Override
			public long run(Dispatcher<TimeEvent> dispatcher, String type, TimeEvent event) {
				fParticles.update(event.delta());
				return Minion.CONTINUE;
			}
		};
	}

	public ParticleLayer add(ParticleEmitter e) {
		fParticles.addEmitter(e);
		return this;
	}
	
	@Override
	public ISavable snapshot() throws CloneNotSupportedException {
		// FIXME: Work this out somehow!
		return (ISavable) super.clone();
	}

	@Override
	public void cleanup() {
		fParticles.removeAllEmitters();
		fParticles = null;
	}
	
	@Override
	protected void setScene(Scene scene) {
		if (fScene != null) timeManager().dispatcher().removeMinion(TimeEvent.TYPE, fUpdater);
		super.setScene(scene);
		if (fScene != null) timeManager().dispatcher().addMinion(TimeEvent.TYPE, fUpdater);
		
	}
	
	@Override
	public void render(Graphics graphics, long currentTime) {
		super.render(graphics, currentTime);
		fParticles.render();
	}
	
	@Override
	public String toString() {
		return "[ParticleLayer #" + fID + "]";
	}
}
