package com.pointcliki.input;

import org.lwjgl.input.Keyboard;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.Minion;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.movement.IArrowKeyMovable;

public class ArrowKeyMover {
	
	private boolean fEnabled = true;
	
	public ArrowKeyMover(InputManager manager, final IArrowKeyMovable movable) {
		Minion<KeyEvent> min = new Minion<KeyEvent>() {

			@Override
			public long run(Dispatcher<KeyEvent> dispatcher, String type, KeyEvent event) {
				if (!fEnabled) return Minion.CONTINUE;
				if (event.is(Keyboard.KEY_UP)) movable.move(GridCoordinate.NORTH);
				else if (event.is(Keyboard.KEY_RIGHT)) movable.move(GridCoordinate.EAST);
				else if (event.is(Keyboard.KEY_DOWN)) movable.move(GridCoordinate.SOUTH);
				else if (event.is(Keyboard.KEY_LEFT)) movable.move(GridCoordinate.WEST);
				return Minion.CONTINUE;
			}
			
		};
		manager.keyDispatcher().addMinion("key.down", min);
		
		min = new Minion<KeyEvent>() {

			@Override
			public long run(Dispatcher<KeyEvent> dispatcher, String type, KeyEvent event) {
				if (event.is(Keyboard.KEY_UP)) movable.stop(GridCoordinate.NORTH);
				else if (event.is(Keyboard.KEY_RIGHT)) movable.stop(GridCoordinate.EAST);
				else if (event.is(Keyboard.KEY_DOWN)) movable.stop(GridCoordinate.SOUTH);
				else if (event.is(Keyboard.KEY_LEFT)) movable.stop(GridCoordinate.WEST);
				return Minion.CONTINUE;
			}
			
		};
		manager.keyDispatcher().addMinion("key.up", min);
	}

	public void enable() {
		fEnabled = true;
	}
	
	public void disable() {
		fEnabled = false;
	}
}

