package com.pointcliki.input;

import java.awt.Point;

import org.lwjgl.input.Keyboard;

import com.pointcliki.core.Manager;
import com.pointcliki.event.Dispatcher;
import com.pointcliki.input.KeyEvent;
import com.pointcliki.input.MouseEvent;

/**
 * The input manager class listens for key presses and mouse motions, and
 * dispatches events to listeners when these occur. Typically you won't use
 * this class in your game directly to detect key presses. Instead, you should
 * use a Player class, which interprets what the key presses mean depending on
 * the user's settings and dispatches action events on behalf of the user which
 * are remembered in the case of network lag.
 * 
 * @author Hugheth
 * @since 3
 */
public class InputManager extends Manager {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 824632761557541127L;
	
	protected Point fMousePosition;
	protected boolean[] fMouseButtons;
	protected int[] fMouseDrag = new int[] {0, 0, 0, 0};
	protected KeyEventDispatcher fKeyDispatcher;
	protected MouseEventDispatcher fMouseDispatcher;
	
	/**
	 * Create a new input manager instance
	 */
	public InputManager() {
		super(null);
		fMouseButtons = new boolean[] {false, false, false};
		fMousePosition = new Point(0, 0);
		fKeyDispatcher = new KeyEventDispatcher();
		fMouseDispatcher = new MouseEventDispatcher();
	}

	@Override
	public void restore(Manager from) {
		// Do nothing as input manager is singleton
	}

	@Override
	public void cleanup() {
		fMouseButtons = null;
		fMousePosition = null;
		fMouseDrag = null;
		fKeyDispatcher = null;
	}
	
	public void addKey(int key, char c) {
		fKeyDispatcher.dispatchKeyEvent("down", key, c);
	}

	public void removeKey(int key, char c) {
		fKeyDispatcher.dispatchKeyEvent("up", key, c);
	}
		
	public void mousePressed(int button, int x, int y) {
		fMouseDrag[0] = x;
		fMouseDrag[1] = y;
		fMouseDrag[2] = x;
		fMouseDrag[3] = y;
		fMouseDispatcher.dispatchMouseEvent("down", x, y, button);
		fMouseDispatcher.dispatchMouseEvent("down." + button, x, y, button);
	}
	
	public void mouseMoved(int ox, int oy, int x, int y) {
		fMousePosition.x = x;
		fMousePosition.y = y;
		fMouseDispatcher.dispatchMouseEvent("move", ox, oy, x, y, -1);
	}
	
	public void mouseDragged(int ox, int oy, int x, int y) {
		fMouseDrag[2] = x;
		fMouseDrag[3] = y;
		fMouseDispatcher.dispatchMouseEvent("drag", ox, oy, x, y, -1);
	}

	public void mouseReleased(int button, int x, int y) {
		fMouseDispatcher.dispatchMouseEvent("up", x, y, button);
		fMouseDispatcher.dispatchMouseEvent("up." + button, x, y, button);
	}

	public void mouseClicked(int button, int x, int y, int count) {
		fMousePosition.x = x;
		fMousePosition.y = y;
		fMouseDispatcher.dispatchMouseEvent("click", x, y, button);
		fMouseDispatcher.dispatchMouseEvent("click." + button, x, y, button);
	}
	
	/**
	 * @return The key dispatcher for this manager
	 */
	public KeyEventDispatcher keyDispatcher() {
		return fKeyDispatcher;
	}
	/**
	 * @return The mouse dispatcher for this manager
	 */
	public MouseEventDispatcher mouseDispatcher() {
		return fMouseDispatcher;
	}
	
	/**
	 * @param key The key to check
	 * @return Whether the key given is pressed
	 */
	public boolean isKeyPressed(int key) {
		return Keyboard.isKeyDown(key);
	}
	
	public boolean isShiftPressed() {
		return Keyboard.isKeyDown(42);
	}
	
	public boolean isAltPressed() {
		return Keyboard.isKeyDown(56);
	}
	
	public boolean isCtrlPressed() {
		return Keyboard.isKeyDown(29);
	}
	
	/**
	 * @param button The button to check
	 * @return Whether the button given is pressed
	 */
	public boolean isMousePressed(int button) {
		if (button < fMouseButtons.length)
			return fMouseButtons[button];
		return false;
	}

	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		InputManager clone = (InputManager) super.clone();
		
		return clone;
	}
	
	/**
	 * A dispatcher to dispatch key events
	 * @author Hugheth
	 * @since 3
	 */
	public class KeyEventDispatcher extends Dispatcher<KeyEvent> {

		/**
		 * Serial key
		 */
		private static final long serialVersionUID = -3335242640820477969L;

		protected void dispatchKeyEvent(String type, int key, char c) {
			dispatchEvent("key." + type, new KeyEvent(key, c));
		}
	}
	
	/**
	 * A dispatcher to dispatch button events
	 * @author Hugheth
	 * @since 3
	 */
	public class MouseEventDispatcher extends Dispatcher<MouseEvent> {
		/**
		 * Serial key
		 */
		private static final long serialVersionUID = -3335242640820477969L;
		
		protected void dispatchMouseEvent(String type, int x, int y, int button) {
			dispatchEvent("mouse." + type, new MouseEvent(x, y - 1, button));
		}
		
		protected void dispatchMouseEvent(String type, int ox, int oy, int x, int y, int button) {
			dispatchEvent("mouse." + type, new MouseEvent(ox, oy - 1, x, y - 1, button));
		}
	}
}

