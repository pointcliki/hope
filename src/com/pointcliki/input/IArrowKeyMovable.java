package com.pointcliki.input;

import com.pointcliki.grid.GridCoordinate;

public interface IArrowKeyMovable {

	/**
	 * Move the entity with the velocity indicated.
	 * @param velocity The velocity to move the entity with
	 */
	public void move(GridCoordinate velocity);
	
	/**
	 * Stop the entity moving in the direction indicated.
	 * @param direction The direction to stop motion in
	 */
	public void stop(GridCoordinate velocity);
}

