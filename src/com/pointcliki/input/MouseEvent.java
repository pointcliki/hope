package com.pointcliki.input;

import com.pointcliki.event.IEvent;

public class MouseEvent implements IEvent {

	private int fX;
	private int fY;
	private int fOldX;
	private int fOldY;
	private int fButton;

	public MouseEvent(int x, int y, int button) {
		fX = x;
		fY = y;
		fButton = button;
	}
	
	public MouseEvent(int ox, int oy, int x, int y, int button) {
		this(x, y, button);
		fOldX = ox;
		fOldY = oy;
	}
	
	public int x() {
		return fX;
	}
	public int y() {
		return fY;
	}
	public int button() {
		return fButton;
	}
	public int oldX() {
		return fOldX;
	}
	public int oldY() {
		return fOldY;
	}
}
