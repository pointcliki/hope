package com.pointcliki.input;

import com.pointcliki.event.IEvent;

/**
 * @author Hugheth
 * @since 3
 */
public class KeyEvent implements IEvent {
	
	protected int fKey;
	protected char fC;

	public KeyEvent(int key, char c) {
		fKey = key;
		fC = c;
	}
	
	public boolean is(int key) {
		return fKey == key;
	}
	
	public int key() {
		return fKey;
	}
	
	public char c() {
		return fC;
	}
}
