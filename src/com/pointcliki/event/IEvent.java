package com.pointcliki.event;

import com.pointcliki.core.IStateless;

/**
 * The IEvent interface is a marker interface to represent an object
 * that doesn't maintain any game state. This is important in identifying the
 * objects that do not have to be cloned during a snapshot. 
 * 
 * @author Hugheth
 * @since 3
 */
public interface IEvent extends IStateless {
	// Marker interface needs no entries
}
