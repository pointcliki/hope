package com.pointcliki.event;

import com.pointcliki.core.Entity;

public abstract class PeriodicRogue<T extends Entity> extends Rogue<T> {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 7760043081081675786L;
	
	private long fPeriod;
	private Minion<FrameEvent> fMinion;
	private long fNextFrame;
	private long fDelay;
	
	public PeriodicRogue(T entity, Minion<ProgressEvent<T>> monitor, long period, long delay) {
		super(entity, monitor);
		fPeriod = period;
		fDelay = delay;
		fMinion = new Minion<FrameEvent>() {
			public long run(com.pointcliki.event.Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
				if (!isRunning()) return Minion.FINISH;
				PeriodicRogue.this.run(fParent, event.frame());
				fNextFrame = event.frame() + fPeriod;
				return fPeriod;
			};	
		};
	}

	/**
	 * This method starts the rouge's execution.
	 */
	public void begin() {
		super.begin();
		fParent.frameManager().queue(fMinion, fDelay);
	}
	
	/**
	 * This method is called by the rogue when it has finished execution, and
	 * sends the response to the monitor.
	 */
	protected void end() {
		super.end();
		fParent.frameManager().dequeue(fMinion, fNextFrame);
	}
	
	/**
	 * This method is called to cancel the rogue's execution. 
	 */
	public void cancel() {
		super.cancel();
		fParent.frameManager().dequeue(fMinion, fNextFrame);
	}
	
	public void period(long p) {
		fPeriod = p;
	}
	
	public void delay(long d) {
		fDelay = d;
	}
	
	@Override
	public void cleanup() {
		super.cleanup();
		fMinion = null;
	}
	
	public abstract void run(T entity, long currentFrame);
}
