package com.pointcliki.event;

import com.pointcliki.core.IStateless;

/**
 * The Minion class is a event listener class that
 * can be registered to any number of Dispatcher instances.
 * 
 * It is used unanimously throughout the engine, especially
 * in dealing with game logic where it should be registered
 * with a scene's FrameManager.
 * 
 * 
 * @author Hugheth
 * @since 3
 * @see Dispatcher
 * @see FrameManager
 * 
 * @param <T> The event class that this minion handles
 */
public class Minion<T extends IEvent> implements IStateless, Comparable<Minion<T>> {

	/**
	 * Indicates that the Minion should no longer be registered through the dispatcher
	 */
	public static final int FINISH = -1;
	/**
	 * Indicates that the Minion should retry immediately for the same event
	 */
	public static final int RETRY = 0;
	/**
	 * Indicates that the Minion should continue to use the dispatcher
	 * for next time an event of the same type is dispatched
	 */
	public static final int CONTINUE = 1;
	
	protected long fID = 0;
	protected int fPriority = 0;
	
	protected static long sUID = 0;
	
	/**
	 * Create a new minion with default priority
	 */
	public Minion() {
		this(0);
	}

	/**
	 * Create a new minion
	 * @param priority The priority of the minion
	 */
	public Minion(int priority) {
		fPriority = priority;
		fID = sUID++;
	}
	
	public long id() {
		return fID;
	}
	
	/**
	 * Run is called by a dispatcher when the minion needs to be run.
	 * 
	 * @param dispatcher The dispatcher that is running this minion
	 * @param type The type of that the dispatcher
	 * @param event The event being dispatched
	 * @return The return code of the minion's execution
	 */
	public long run(Dispatcher<T> dispatcher, String type, T event) {
		return FINISH;
	};

	/**
	 * Compare the minion to another. This is used to work out which
	 * minion has priority over another. This will never let minions
	 * equal one another, otherwise issues arise when people use minions
	 * of the same priority.
	 * 
	 * This also means that you can register a minion multiple times to
	 * a dispatcher's event set, due to the implementation of TreeSet.
	 */
	@Override
	public int compareTo(Minion<T> o) {
		if (((Minion<? extends IEvent>) o).fPriority > fPriority) return -1;
		return 1;
	}
}

