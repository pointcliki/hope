package com.pointcliki.event;

import java.util.ArrayList;

import com.pointcliki.core.Entity;

/**
 * @author Hugheth
 * @since 3
 *
 * @param <T> The type of entity used by the rogues
 */
public class Parallel<T extends Entity> extends Rogue<T> {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -1303832519063266674L;
	
	/**
	 * List of rogues to execute at the same time
	 */
	protected ArrayList<Rogue<T>> fList;
	protected int size = -1;
	
	/**
	 * Create a new parallel
	 * @param parent The acting parent of the rogue, which child rogues don't have to be parented to
	 * @param monitor The monitor to receive progress events
	 */
	public Parallel(T parent, Minion<ProgressEvent<T>> monitor) {
		super(parent, monitor);
	}
	
	/**
	 * @param rogue The rogue to add to the parallel execution list
	 * @return The Parallel instance
	 */
	public Parallel<T> add(Rogue<T> rogue) {
		return add(rogue, false);
	}
	
	/**
	 * @param rogue The rogue to add to the parallel execution list
	 * @param force If true, the rogue will be added even if the Parallel instance has been started. This may cause race conditions.
	 * @return The Parallel instance
	 */
	public Parallel<T> add(Rogue<T> rogue, boolean force) {
		if (!force) assert (size == -1): "Can't add any more rogues as the sequence has started!";
		fList.add(rogue);
		return this;
	}
	
	// TODO: Write parallel class
}
