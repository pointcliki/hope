package com.pointcliki.event;

import java.util.TreeSet;

import com.pointcliki.core.Manager;
import com.pointcliki.core.Scene;
import com.pointcliki.core.TimeManager;

/**
 * The frame manager handles dispatching of events for each logical frame of
 * a scene
 * @author Hugheth
 * @since 3
 * @see Minion
 * @see TimeManager
 */
public class FrameManager extends Manager {

	/**
	 * Serial Key
	 */
	private static final long serialVersionUID = 1738455447538855777L;
	
	protected int fMaxIterations = 1;
	protected FrameEvent fCurrentEvent = new FrameEvent(0);
	protected Dispatcher<FrameEvent> fDispatcher;

	/**
	 * Create a new frame manager
	 * @param scene The scene the frame manager controls
	 */
	public FrameManager(Scene scene) {
		super(scene);
		fDispatcher = new Dispatcher<FrameEvent>() {
			
			/**
			 * Serial key
			 */
			private static final long serialVersionUID = 4680046045945606140L;

			@Override
			protected void handleReturn(Minion<FrameEvent> minion, TreeSet<Minion<FrameEvent>> set, TreeSet<Minion<FrameEvent>> retry, long result) {
				if (result == Minion.FINISH) set.remove(minion);
				else if (result == Minion.RETRY) retry.add(minion);
				else queue(minion, result);
			}
		};
	}

	/**
	 * Indicate a frame has passed
	 * @param currentFrame The current frame
	 */
	public void tick(long currentFrame) {
		fCurrentEvent = new FrameEvent(currentFrame);
		fDispatcher.dispatchIterativeEvent("frame." + currentFrame, fCurrentEvent, fMaxIterations);		
		fDispatcher.releaseMinions("frame." + currentFrame);
	}
	
	/**
	 * Add a minion to run in the next frame
	 * @param minion The minion to queue
	 */
	public void queueNext(Minion<FrameEvent> minion) {
		queue(minion, 1);
	}
	
	/**
	 * Add a minion to run after a number of frames delay
	 * @param minion The minion to queue
	 * @param framesFromNow The number of frames to elapse before calling this minion
	 */
	public void queue(Minion<FrameEvent> minion, long framesFromNow) {
		framesFromNow = Math.max(1, framesFromNow);
		fDispatcher.addMinion("frame." + (fParent.manager(TimeManager.class).currentFrame() + framesFromNow), minion);
	}

	/**
	 * Dequeues an minion to stop it from running in the future 
	 * @param toRemove The minion to remove from the event manager
	 * @param frame The frame at which to remove the minion
	 */
	public void dequeue(Minion<FrameEvent> toRemove, long frame) {
		fDispatcher.removeMinion("frame." + frame, toRemove);
	}
	
	/**
	 * Just use a constant 
	 */
	@Override
	public int hashCode() {
		return (int) (serialVersionUID % Integer.MAX_VALUE);
	}

	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		FrameManager clone = (FrameManager) super.clone();
		clone.fDispatcher = fDispatcher.snapshot();
		return clone;
	}

	@Override
	public void restore(Manager from) {
		fDispatcher = ((FrameManager)from).fDispatcher;
	}

	@Override
	public void cleanup() {
		fDispatcher.cleanup();
		fCurrentEvent = null;
	}
}

