package com.pointcliki.event;

import com.pointcliki.core.Entity;

/**
 * A ProgressEvent instance is passed to monitors of Rogues to keep them
 * updated about the rogue's progress. When extending the rogue class to
 * perform some functionality, you can extend this class to pass 
 * customised information to the monitor.
 * 
 * @author Hugheth
 * @since 3
 *
 * @param <T> The entity the rogue is attached to
 */
public class ProgressEvent<T extends Entity> implements IEvent {
	protected Rogue<T> fRogue;
	/**
	 * @param rogue The originator of this event
	 */
	public ProgressEvent(Rogue<T> rogue) {
		fRogue = rogue;
	}
	/**
	 * @return Return the rogue that created this event
	 */
	public Rogue<T> rogue() {
		return fRogue;
	}
}