package com.pointcliki.event;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import com.pointcliki.core.ISavable;

/**
 * A Dispatcher dispatches events to registered minions that
 * are interested in the type of event being dispatched.
 * 
 * To use this class, extend it and expose a dispatch event
 * method that calls the protected dispatchEvent method, thus
 * preserving encapsulation.
 * 
 * This class is useful when in general you have a number of
 * possible events that a parent class will throw, and you want to
 * allow other people to interact with the parent class in different
 * ways. 
 * 
 * Implementing classes should use string lookup that makes
 * use of the hashcode of the string (e.g. using a HashMap),
 * which is usually only worked out once, so in general is as fast as
 * integer lookup while allowing a wider range of extensibility. 
 *  
 * @author hugheth
 * @since 3
 * @see IEvent
 * @see Minion
 * 
 * @param <T> The event class that this minion handles
 */
public class Dispatcher<T extends IEvent> implements ISavable {
	
	/**
	 * Serial Key
	 */
	private static final long serialVersionUID = -944138350897615021L;
	
	protected HashMap<String, TreeSet<Minion<T>>> fMinions = new HashMap<String, TreeSet<Minion<T>>>();

	@SuppressWarnings("unchecked")
	@Override
	public Dispatcher<T> snapshot() throws CloneNotSupportedException {
		Dispatcher<T> clone = (Dispatcher<T>) super.clone();
		clone.fMinions = (HashMap<String, TreeSet<Minion<T>>>) fMinions.clone();
		return clone;
	}

	/**
	 * Cleans up the dispatcher when it is no longer needed
	 */
	public void cleanup() {
		fMinions = null;
	}

	/**
	 * Add the minion to the dispatcher. No checks are done
	 * on whether the minion has already been added to the
	 * dispatcher. If it has, the minion will be called multiple
	 * times.
	 * 
	 * @param type The type of event
	 * @param minion The minion to receive events
	 */
	public void addMinion(String type, Minion<T> minion) {
		if (!fMinions.containsKey(type)) fMinions.put(type, new TreeSet<Minion<T>>());
		fMinions.get(type).add(minion);
	}
	
	/**
	 * Removes the minion from the dispatcher, for the current
	 * event type. If the minion was added more than once to this
	 * event type, only one of the registrations will be removed.
	 * 
	 * @param type The type of event the minion was listening to
	 * @param minion The minion to stop receiving events
	 * @return Whether a minion was removed
	 */
	public boolean removeMinion(String type, Minion<T> minion) {
		if (!fMinions.containsKey(type)) return false;
		// Need to iterate as remove won't work on TreeSet as it uses compareTo to check for equality
		Iterator<Minion<T>> it = fMinions.get(type).iterator();
		while (it.hasNext()) if (it.next().id() == minion.id()) {
			it.remove();
			return true;
		}
		return false;
	}
	
	/**
	 * @param type The type of event
	 * @param minion The minion to check for
	 * @return True if this minion is registered with the dispatcher for the event type one or more times.
	 */
	public boolean hasMinion(String type, Minion<T> minion) {
		if (!fMinions.containsKey(type)) return false;
		return fMinions.get(type).contains(minion);
	}

	/**
	 * @param type The type of event
	 * @return The set of minions registered to the given type
	 */
	public Set<Minion<T>> minions(String type) {
		if (!fMinions.containsKey(type)) return new TreeSet<Minion<T>>();
		return fMinions.get(type);
	}
	
	/**
	 * Removes all of the minions associated with the given type
	 * 
	 * @param type The type of event
	 */
	public void releaseMinions(String type) {
		if (fMinions.containsKey(type)) fMinions.remove(type);
		fMinions.remove(type);
	}
	
	/**
	 * Dispatch an event to registered minions
	 * @param type The type of the event 
	 * @param event The event object, implementing IEvent
	 */
	protected void dispatchEvent(String type, T event) {
		dispatchIterativeEvent(type, event, 1);
	}
	
	/**
	 * Dispatches an iterative event to registered minions.
	 * This means that after all the registered minions have been
	 * run, if some have returned Minion.RETRY as a status, they
	 * will be re-run. This iteration continues up to maxIterations or
	 * when no minions are attempting to retry. 
	 * @param type The type of event being dispatched
	 * @param event The event object being dispatched
	 * @param maxIterations The maximium number of iterations to use
	 * @return Returns the number of iterations used, or -1 if the event wasn't dispatched
	 */
	protected int dispatchIterativeEvent(String type, T event, int maxIterations) {
		if (!fMinions.containsKey(type)) return -1;
		// Ensure positive number of iterations
		assert maxIterations > -1;
		
		// Get the set of minions to dispatch the event to
		TreeSet<Minion<T>> set = fMinions.get(type);
		
		// Use a clone of the set so minions executing in the loop can modify the underlying set
		@SuppressWarnings("unchecked")
		TreeSet<Minion<T>> clone = (TreeSet<Minion<T>>) set.clone();
		
		// Keep track of the current set to iterate over
		TreeSet<Minion<T>> iterateOver = clone;
		
		int i = maxIterations;
		
		// Iterate
		while (i > 0) {
			// Keep a list of minions to re-run
			TreeSet<Minion<T>> retry = new TreeSet<Minion<T>>();
					
			// Iterate through minions and dispatch
			for (Minion<T> minion: iterateOver) {
				try {
					handleReturn(minion, set, retry, minion.run(this, type, event));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			// Check for retries
			if (retry.isEmpty()) break;
			// Iterate over the retries next iteration
			iterateOver = retry;
			
			i--;
		}
		return i;
	}
	
	protected void handleReturn(Minion<T> minion, TreeSet<Minion<T>> set, TreeSet<Minion<T>> retry, long result) {
		if (result == Minion.FINISH) set.remove(minion);
		else if (result == Minion.RETRY) retry.add(minion);
	}
}

