package com.pointcliki.event;

/**
 * 
 * @author Hugheth
 * @since 3
 */
public class FrameEvent implements IEvent {
	
	private long fCurrentFrame;
	
	/**
	 * Create a new frame event
	 * @param currentFrame The current frame of the scene
	 */
	public FrameEvent(long currentFrame) {
		fCurrentFrame = currentFrame;
	}
	/**
	 * @return The current frame of the scene
	 */
	public long frame() {
		return fCurrentFrame;
	}
}
