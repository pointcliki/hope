package com.pointcliki.event;

import com.pointcliki.core.Entity;
import com.pointcliki.core.ISavable;

/**
 * A rogue is a class that performs some sort of asynchronous work for an
 * entity, for example, playing a sound or displaying a movement transition.
 * 
 * Only one reference of this class should be kept if the rogue is used inside
 * a scene, and that is by the entity that the rogue is parented to. This is so
 * that when a snapshot is taken, only one clone of the rogue is made.
 * 
 * When a snapshot is made of an entity, references to rogues attached to it
 * should be reset by calling rogue.snapshot(entity), allowing the rogue to
 * pick up the new clone of the entity
 * 
 * @author Hugheth
 * @since 3
 * 
 * @see Minion
 * @see Dispatcher
 *
 * @param <T> The type of entity that the rogue is to be parented to
 */
public abstract class Rogue<T extends Entity> implements ISavable {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 7760043081081675786L;

	protected T fParent;
	protected Minion<ProgressEvent<T>> fMonitor;
	protected String fType;
	protected boolean fRunning = false;
	protected boolean fCanceled = false;
	protected boolean fDone = false;
	protected boolean fPaused = false;
	protected String fHookType;
	protected int fHookReturnValue;
	protected Minion<IEvent> fHook = null;

	/**
	 * The type of event sent to the monitor when the rogue begins
	 */
	public static String ON_BEGIN = "begin";
	/**
	 * The type of event sent to the monitor when the rogue ends
	 */
	public static String ON_END = "end";
	/**
	 * The type of event sent to the monitor when the rogue is cancelled
	 */
	public static String ON_CANCEL = "cancel";
	/**
	 * The type of event sent to the monitor when the rogue is paused
	 */
	public static final String ON_PAUSE = "pause";
	/**
	 * The type of event sent to the monitor when the rogue is resumed
	 */
	public static final String ON_RESUME = "pause";
	
	/**
	 * Create a new rogue
	 * @param entity The parent entity
	 * @param monitor The monitor minion which listens to events dispatched by
	 * the rogue
	 */
	public Rogue(T entity, Minion<ProgressEvent<T>> monitor) {
		fParent = entity;
		if (monitor == null) fMonitor = new Minion<ProgressEvent<T>>(0) {
			@Override
			public long run(Dispatcher<ProgressEvent<T>> dispatcher, String type, ProgressEvent<T> event) {
				return 0;
			}
		};
		else fMonitor = monitor;
	}

	/**
	 * This method starts the rouge's execution.
	 */
	public void begin() {
		assert !fRunning: "Can't start a rogue that has already started";
		fRunning = true;
		fCanceled = false;
		fDone = false;
		fPaused = false;
		fMonitor.run(null, ON_BEGIN, new ProgressEvent<T>(this));
	}
	
	/**
	 * This method is called by the rogue when it has finished execution, and
	 * sends the response to the monitor.
	 */
	protected void end() {
		assert fRunning: "Can't stop a rogue that hasn't started";
		fRunning = false;
		fPaused = false;
		fDone = true;
		fMonitor.run(null, ON_END, new ProgressEvent<T>(this));
	}
	
	/**
	 * This method is called to cancel the rogue's execution. 
	 */
	public void cancel() {
		assert fRunning: "Can't cancel a rogue that hasn't started";
		fRunning = false;
		fDone = true;
		fCanceled = true;
		fPaused = false;
		fMonitor.run(null, ON_CANCEL, new ProgressEvent<T>(this));
	}
	
	/**
	 * Pause the rogue at the current point of time
	 */
	public void pause() {
		assert fRunning && !fPaused: "Can't pause a rogue that hasn't started";
		fRunning = false;
		fDone = false;
		fCanceled = false;
		fPaused = true;
		fMonitor.run(null, ON_PAUSE, new ProgressEvent<T>(this));
	}
	
	/**
	 * Continue the rogue's execution
	 */
	public void resume() {
		assert fPaused: "Can't resume a rogue that hasn't been paused";
		fRunning = true;
		fPaused = false;
		fMonitor.run(null, ON_RESUME, new ProgressEvent<T>(this));
	}
	
	/**
	 * @return Whether the rogue has been completed
	 */
	public boolean isDone() {
		return fDone;
	}
	/**
	 * @return Whether the rogue has been cancelled
	 */
	public boolean isCanceled() {
		return fCanceled;
	}
	/**
	 * @return Whether the rogue is currently running
	 */
	public boolean isRunning() {
		return fRunning;
	}
	/**
	 * @return Whether the rogue is currently paused
	 */
	public boolean isPaused() {
		return fPaused;
	}
	
	/**
	 * A hook can be used to link the rogue with the parent entity's dispatcher
	 * so that the rogue is automatically begun when the dispatcher dispatches
	 * the event specified.
	 *  
	 * @param type The type of event to listen for  
	 * @param priority The priority of the minion to use
	 * @param returnValue The return value of the minion
	 * @return The rogue instance
	 */
	public Rogue<T> hook(String type, int priority, final int returnValue) {
		if (fHook == null) {
			fHook = new Minion<IEvent>(priority) {
				@Override
				public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
					// The default behaviour of the hook is to begin the rogue's
					// execution
					Rogue.this.begin();
					return returnValue;
				}
			};
		} else {
			// Remove if it's already been added to the dispatcher
			dehook();
		}
		// Add to the dispatcher
		fHookType = type;
		fHookReturnValue = returnValue;
		fParent.dispatcher().addMinion(type, fHook);
		return this;
	}
	
	/**
	 * Decouple the hook with the entity's dispatcher
	 * @return The rogue instance
	 */
	public Rogue<T> dehook() {
		assert fHook != null: "Can't dehook when the hook hasn't been created!";
			fParent.dispatcher().removeMinion(fHookType, fHook);
		fHook = null;
		return this;
	}

	@Override
	public ISavable snapshot() throws CloneNotSupportedException {
		throw new CloneNotSupportedException("Need to use snapshot(entity) to take a snapshot of a rogue");
	}
	
	/**
	 * Take a snapshot of the rogue, linking up the clone to the snapshot of
	 * the entity it is parented to. This method should be called instead of
	 * snapshot() by the parent entity during its snapshot method.
	 * @param entity The snapshot of the parent entity
	 * @return A snapshot of the rogue
	 * @throws CloneNotSupportedException If the clone isn't supported
	 */
	public ISavable snapshot(T entity) throws CloneNotSupportedException {
		@SuppressWarnings("unchecked")
		Rogue<T> clone = (Rogue<T>) super.clone();
		clone.fParent = entity;
		// Check whether the hook needs to be attached to the new entity
		if (fHook != null) {
			dehook();
			hook(fHookType, fHook.fPriority, fHookReturnValue);
		}
		return clone;
	}
	
	
	/**
	 * @return The entity that the rogue is parented to
	 */
	public T entity() {
		return fParent;
	}
	
	public void cleanup() {
		if (fRunning) cancel();
		if (fHook != null) dehook();
	}
}
