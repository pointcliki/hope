package com.pointcliki.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.PointClikiGame;
import com.pointcliki.event.Minion;
import com.pointcliki.input.MouseEvent;

public class Button extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 1952765978554324331L;
	
	protected Minion<SelectionEvent> fMonitor;
	protected String fText;
	protected UnicodeFont fFont;
	protected int fOffset;
	
	public Button(Minion<SelectionEvent> monitor, String txt) {
		fSpan = new Rectangle(0, 0, 200, 12);
		fMonitor = monitor;
		fFont = PointClikiGame.resourceManager().UIFont();
		text(txt);
	}
	
	public void text(String text) {
		fText = text;
		fOffset = (int) (fSpan.getWidth() / 2 - fFont.getWidth(fText) / 2);
	}
	
	@Override
	public Button resize(Vector2f span) {
		super.resize(span);
		fOffset = (int) (fSpan.getWidth() / 2 - fFont.getWidth(fText) / 2);
		return this;
	}
	
	public void handleUIMouseEvent(String type, Vector2f local, MouseEvent event) {
		fDispatcher.dispatchEvent(type, event);
		if (type.equals("mouse.click")) fMonitor.run(null, "click", null);
	}
	
	@Override
	public void render(Graphics graphics, long currentTime) {
		super.render(graphics, currentTime);
		if (fHovering) graphics.setColor(new Color(180, 180, 180));
		else graphics.setColor(Color.gray);
		graphics.fillRect(0, 0, fSpan.getWidth(), fSpan.getHeight());	
		fFont.drawString(fOffset, 3, fText, Color.white);
	}
}
