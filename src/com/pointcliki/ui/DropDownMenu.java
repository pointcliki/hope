package com.pointcliki.ui;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.PointClikiGame;
import com.pointcliki.event.Minion;
import com.pointcliki.input.MouseEvent;

public class DropDownMenu extends UIEntity {
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 4202047744799990949L;
	
	protected Minion<SelectionEvent> fMonitor;
	protected ArrayList<String> fElements;
	protected int fSelected = 0;
	protected Image fImage;
	protected boolean fOpen = false;
	protected int fHoveredOption = 0;
	protected String fDefault;
	
	protected static Color sHoverColor = new Color(120, 160, 255);
	protected static Color sOddColor = new Color(220, 220, 220);
	protected static Color sEvenColor = new Color(240, 240, 240); 
	
	public DropDownMenu(Minion<SelectionEvent> monitor) {
		fMonitor = monitor;
		fElements = new ArrayList<String>();
		fSpan = new Rectangle(0, 0, 200, 28);
		try {
			fImage = new Image("lib/ui/dropdownmenu-button.png");
		} catch (Exception e) {
			System.err.println("Couldn't find drop down menu button image");
		}
	}
	
	public DropDownMenu add(String s) {
		fElements.add(s);
		return this;
	}
	
	public DropDownMenu select(int index) {
		fSelected = index;
		return this;
	}
	
	public DropDownMenu select(String n) {
		int i = 0;
		for (String s: fElements) {
			if (s.equals(n)) {
				fSelected = i;
				break;
			}
			i++;
		}
		return this;
	}
	public String value() {
		return fElements.get(fSelected);
	}
	
	@Override
	public void blur() {
		super.blur();
		close();
	}
	
	public void handleUIMouseEvent(String type, Vector2f local, MouseEvent event) {
		fDispatcher.dispatchEvent(type, event);
		
		// Click
		if ((type.equals("blur") || type.equals("mouse.down")) && fFocused) {
			if (!fOpen) {
				fOpen = true;
				fSpan.setHeight(fSpan.getHeight() * (fElements.size() + 1));
			} else {
				fSelected = fHoveredOption;
				close();
			}
		}
		if (fOpen && type.equals("mouse.move")) {
			int size = (int) (fSpan.getHeight() / (fElements.size() + 1));
			fHoveredOption = (int) (local.y / size) - 1;
			if (fHoveredOption == -1) fHoveredOption = fSelected;
		}
	}
	
	protected void close() {
		if (!fOpen) return;
		fOpen = false;
		fSpan.setHeight(fSpan.getHeight() / (fElements.size() + 1));
		fMonitor.run(null, "select", new SelectionEvent(this, fSelected));
	}
	
	@Override
	public void render(Graphics graphics, long currentTime) {
		super.render(graphics, currentTime);
		graphics.setColor(Color.white);
		graphics.fillRect(0, 0, fSpan.getWidth(), fSpan.getHeight());
		if (fImage != null) fImage.draw(140, 8);
		if (fElements.size() == 0) return;
		Color c;
		if (fHovering) c = new Color(0, 80, 120);
		else c = new Color(0, 0, 0);
		PointClikiGame.resourceManager().UIFont().drawString(5, 5, fElements.get(fSelected), c);
		if (fOpen) {
			int size = (int) (fSpan.getHeight() / (fElements.size() + 1));
			for (int i = 0; i < fElements.size(); i++) {
				if (i == fHoveredOption) graphics.setColor(sHoverColor);
				else if (i % 2 == 0) graphics.setColor(sEvenColor);
				else graphics.setColor(sOddColor);
				graphics.fillRect(0, size * (i + 1), fSpan.getWidth(), size);
				if (i == fHoveredOption) PointClikiGame.resourceManager().UIFont().drawString(5, size * (i + 1) + 5, fElements.get(i), Color.white);
				else PointClikiGame.resourceManager().UIFont().drawString(5, size * (i + 1) + 5, fElements.get(i), Color.black);
			}
		}
	}
	
	public int selected() {
		return fSelected;
	}
}

