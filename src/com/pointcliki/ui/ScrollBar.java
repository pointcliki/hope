package com.pointcliki.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.event.Minion;
import com.pointcliki.input.MouseEvent;

public class ScrollBar extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -4347179484068445655L;
	
	protected Minion<SelectionEvent> fMonitor;
	protected boolean fVertical;
	protected int fTicks = 1;
	protected int fBarSpan = 1;
	protected int fValue = 0;
	protected Vector2f fStart;
	protected int fPrev;
	
	public ScrollBar(Minion<SelectionEvent> monitor, boolean vertical) {
		fMonitor = monitor;
		fVertical = vertical;
		if (vertical)
			fSpan = new Rectangle(0, 0, 12, 200);
		else
			fSpan = new Rectangle(0, 0, 200, 12);
	}
	
	public void ticks(int value) {
		fTicks = value;
	}
	public void barSpan(int value) {
		fBarSpan = value;
	}
	public void value(int val) {
		fValue = Math.max(0, Math.min(fTicks - fBarSpan, val));
	}
	
	public int value() {
		return fValue;
	}

	public void handleUIMouseEvent(String type, Vector2f local, MouseEvent event) {
		fDispatcher.dispatchEvent(type, event);
		
		if (type.equals("mouse.down")) {
			fStart = new Vector2f(event.x(), event.y());
			fPrev = fValue;
		} else if (type.equals("mouse.drag")) {
			// Position
			float offset = 0;
			if (fVertical) offset = event.y() - fStart.y;
			else offset = event.x() - fStart.x;
			
			if (fVertical) offset /= fSpan.getHeight() / fTicks;
			else offset /= fSpan.getWidth() / fTicks;
			
			int prevValue = fValue;
			value((int) offset + fPrev);
			
			if (prevValue != fValue) fMonitor.run(null, "scroll", new SelectionEvent(fValue));
		}
	}
	
	@Override
	public void render(Graphics graphics, long currentTime) {
		super.render(graphics, currentTime);
		graphics.setColor(Color.white);
		graphics.fillRect(0, 0, fSpan.getWidth(), fSpan.getHeight());
		if (fHovering) graphics.setColor(new Color(180, 180, 180));
		else graphics.setColor(Color.gray);
		try {
			if (fVertical)
				graphics.fillRect(0, fSpan.getHeight() * ((float) fValue / (float) fTicks), fSpan.getWidth(), fSpan.getHeight() * ((float) fBarSpan / (float) fTicks));	
			else
				graphics.fillRect(fSpan.getWidth() * ((float) fValue / (float) fTicks), 0, fSpan.getWidth() * ((float) fBarSpan / (float) fTicks), fSpan.getHeight());
		} catch (Exception e) {
			// Ensure doesn't crash if div by 0
		}
	}
	
	@Override
	public String toString() {
		return "[Scrollbar #" + fID + "]";
	}
}
