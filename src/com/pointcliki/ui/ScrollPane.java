package com.pointcliki.ui;

import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.Minion;

public class ScrollPane extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 3922222206746115519L;
	
	private Canvas fCanvas;
	private ScrollBar fScrollBar;

	public ScrollPane(Vector2f s) {
		resize(s);
		
		// Todo: Add horizontal direction
		
		fCanvas = new Canvas(new Vector2f(s.x - 12, s.y));
		addChild(fCanvas);
		
		fScrollBar = new ScrollBar(new Minion<SelectionEvent>() {
			@Override
			public long run(Dispatcher<SelectionEvent> dispatcher, String type, SelectionEvent event) {
				int val = fScrollBar.value();
				fCanvas.offset(new Vector2f(0, val));
				return super.run(dispatcher, type, event);
			}
		}, true);
		fScrollBar.position(new Vector2f(s.x - 12, 0));
		fScrollBar.resize(new Vector2f(12, s.y));
		fScrollBar.ticks(100);
		fScrollBar.barSpan(10);
		addChild(fScrollBar);
	}
	
	@Override
	public UIEntity resize(Rectangle r) {
		// TODO Auto-generated method stub
		return super.resize(r);
	}
	
	public Canvas canvas() {
		return fCanvas;
	}
}
