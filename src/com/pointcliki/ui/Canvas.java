package com.pointcliki.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.Entity;

public class Canvas extends UIEntity {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -4603182249004071007L;
	
	private Image fImage;
	private Vector2f fOffset;
	
	public Canvas(Vector2f size) {
		resize(size);
		fOffset = new Vector2f(0, 0);
	}
	
	public Vector2f offset() {
		return fOffset;
	}
	
	public Canvas offset(Vector2f off) {
		fOffset = off;
		return this;
	}
	
	@Override
	protected void renderEntities(Graphics graphics, long currentTime) {
		// Render the entitys to the image graphics
		Graphics g = graphics();
		g.clear();
		
		graphics.setColor(Color.white);
		graphics.fillRect(0, 0, fSpan.getWidth(), fSpan.getHeight());
		
		for (Entity entity: this) {
			// Cache position in case it moves
			int x = (int) entity.position().getX();
			int y = (int) entity.position().getY();
			int ox = (int) entity.origin().x;
			int oy = (int) entity.origin().y;
			float sx = entity.scale().x;
			float sy = entity.scale().y;
			float rot = entity.rotation();
			// Transform the graphics
			g.translate(x + ox - fOffset.x, y + ox - fOffset.y);
			g.rotate(0, 0, -rot);
			g.scale(sx, sy);
			g.translate(-ox, -oy);
			try {
				entity.render(g, currentTime);
			} finally {
				// Transform the graphics back
				g.translate(ox, oy);
				g.scale(1 / sx, 1 / sy);
				g.rotate(0, 0, rot);
				g.translate(-ox - x + fOffset.x, -oy - y + fOffset.y);
			}
		}
		// Render the image
		graphics.drawImage(fImage, 0, 0);
	}
	
	public Graphics graphics() {
		try {
			if (fImage != null) return fImage.getGraphics();
			return null;
		} catch (SlickException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Canvas resize(Rectangle r) {
		try {
			if (fImage != null) fImage.destroy();
			fImage = new Image((int) r.getWidth(), (int) r.getHeight());
		} catch (SlickException e) {
			e.printStackTrace();
			return null;
		}
		fSpan = new Rectangle(0, 0, r.getWidth(), r.getHeight());
		return this;
	}
	
	public void cleanup() {
		try {
			fImage.destroy();
		} catch (SlickException e) {
			e.printStackTrace();
		}
		fImage = null;
		super.cleanup();
	}
}
