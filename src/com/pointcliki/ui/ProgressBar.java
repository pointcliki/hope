package com.pointcliki.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;


public class ProgressBar extends UIEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -227267439398601123L;
	
	private Color fBack;
	private Color fFront;
	private float fPercent;

	public ProgressBar(Vector2f size, Color back, Color front) {
		fBack = back;
		fFront = front;
		resize(size);
	}
	
	public void percent(float f) {
		fPercent = f;
	}
	
	public float percent() {
		return fPercent;
	}
	
	@Override
	public void render(Graphics graphics, long currentTime) {
		 graphics.setColor(fBack);
		 graphics.fillRect(0, 0, span().getWidth(), span().getHeight());
		 
		 graphics.setColor(fFront);
		 graphics.fillRect(1, 1, (span().getWidth() - 2) * fPercent, span().getHeight() - 2);
	}
}
