package com.pointcliki.ui;

import com.pointcliki.event.IEvent;

/**
 * @author Hugheth
 * @since 3
 */
public class SelectionEvent implements IEvent {

	private int fValue;
	private UIEntity fSource;

	public SelectionEvent(int value) {
		fValue = value;
	}
	
	public SelectionEvent(UIEntity source, int value) {
		fValue = value;
		fSource = source;
	}
	
	public int value() {
		return fValue;
	}
	
	public UIEntity source() {
		return fSource;
	}
}
