package com.pointcliki.map;

import org.json.JSONObject;

public interface IMapFactory {
	public IMapObject createFromJSON(JSONObject obj);
}

