package com.pointcliki.map;

import com.pointcliki.event.IEvent;

public class MapEvent implements IEvent {

	public static final String ACTIVE_ADDED = "activeAdded";
	public static final String ACTIVE_REMOVED = "activeRemoved";
	public static final String ACTIVE_UPDATED = "activeUpdated";
	private IMapObject fObject;

	public MapEvent(IMapObject e) {
		fObject = e;
	}
	
	public IMapObject object() {
		return fObject;
	}

}
