package com.pointcliki.map;

import org.newdawn.slick.tiled.TiledMap;

public class TiledMapMap extends Map {

	TiledMap fTiledMap;
	
	public TiledMapMap(MapManager m, TiledMap map) {
		super(m);
		fTiledMap = map;
	}

	public TiledMap tiledMap() {
		return fTiledMap;
	}

	@Override
	public int width() {
		return fTiledMap.getWidth();
	}

	@Override
	public int height() {
		return fTiledMap.getHeight();
	}

	@Override
	public int tile(int x, int y, int layer) {
		return fTiledMap.getTileId(x, y, layer);
	}
	
	@Override
	public void tile(int x, int y, int layer, int tile) {
		fTiledMap.setTileId(x, y, layer, tile);
	}
}
