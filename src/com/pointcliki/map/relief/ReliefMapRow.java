package com.pointcliki.map.relief;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;

import com.pointcliki.core.Entity;
import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.Minion;
import com.pointcliki.map.MapEvent;
import com.pointcliki.ui.UIEntity;

public class ReliefMapRow extends UIEntity {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -954188668449340249L;

	private ReliefMap fMap;
	private int fRow;
	private ReliefTileSet fTiles;
	private ArrayList<IReliefObject> fActives;
	
	public ReliefMapRow(ReliefMap m, int row, ArrayList<IReliefObject> actives) {
		fMap = m;
		fRow = row;
		fActives = actives;
		fTiles = tileset();
		
		m.dispatcher().addMinion(MapEvent.ACTIVE_UPDATED, new UpdateMinion());
		
		for (IReliefObject o: actives) addChild((Entity) o);
	}
	
	protected ReliefTileSet tileset() {
		return (ReliefTileSet) fMap.tileset(0);
	}
	
	@Override
	public void render(Graphics graphics, long currentTime) {		
		fTiles.startUse();
		for (int x = 0; x < fMap.width(); x++) {
			// Draw tiles
			fTiles.draw(x, fRow, fMap.tile(x, fRow, 0), fMap.wall(x, fRow), fMap.relief(x, fRow + 1), fMap.relief(x, fRow), fMap.decals(x, fRow));
		}
		fTiles.endUse();
		
		super.render(graphics, currentTime);
	}
	
	private class UpdateMinion extends Minion<MapEvent> {
		@Override
		public long run(Dispatcher<MapEvent> dispatcher, String type, MapEvent event) {
			if (event.object() instanceof IReliefObject) {
				IReliefObject g = (IReliefObject) event.object();
				int y = g.getTile().y();
				if (y == fRow && !fActives.contains(g)) {
					fActives.add(g);
					ReliefMapRow.this.addChild((Entity) g);
				} else if (y != fRow && fActives.contains(g)) {
					fActives.remove(g);
					ReliefMapRow.this.removeChild((Entity) g);
				}
			}
			return Minion.CONTINUE;
		}
	}

	public void init() {
		for (IReliefObject r: fActives) r.init(fMap);
	}
}
