package com.pointcliki.map.relief;

import org.newdawn.slick.geom.Vector2f;

public class Decal {
	public String type;
	public Vector2f offset;
	public Decal(String type, Vector2f offset) {
		this.type = type;
		this.offset = offset;
	}
}
