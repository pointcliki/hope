package com.pointcliki.map.relief;

import com.pointcliki.grid.IGridEntity;
import com.pointcliki.map.IMapObject;

public interface IReliefObject extends IGridEntity, IMapObject {

}

