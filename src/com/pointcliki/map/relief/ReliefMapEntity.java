package com.pointcliki.map.relief;

import com.pointcliki.core.EntityQuery;

import com.pointcliki.map.IMapObject;
import com.pointcliki.map.MapEntity;
import com.pointcliki.utils.HashBag;

/**
 * An isometric map entity draws a tiled map as an isometric or 2.5D map
 * 
 * @author Hugheth
 * @since 3
 */
public class ReliefMapEntity extends MapEntity {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 3489791457039849356L;
	
	public EntityQuery<ReliefMapRow> fRows;

	public ReliefMapEntity(ReliefMap m) {
		super(m);
		
		fRows = new EntityQuery<ReliefMapRow>();
		
		// Sort actives into rows
		HashBag<Integer, IReliefObject> actives = new HashBag<Integer, IReliefObject>();
		for (IMapObject e: m.actives()) {
			IReliefObject o = (IReliefObject) e;
			int row = o.getTile().y();
			actives.add(row, o);
		}
		
		// TODO: bind listeners for adding new logics
		
		// Create the rows
		for (int y = 0; y < m.height(); y++) {
			ReliefMapRow row = new ReliefMapRow(m, y, actives.get(y));
			fRows.add(row);
			addChild(row);
		}
	}

	@Override
	public void init() {
		for (ReliefMapRow row: fRows) {
			row.init();
		}
	}
}
