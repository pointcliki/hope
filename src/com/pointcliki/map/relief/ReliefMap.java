package com.pointcliki.map.relief;

import org.json.JSONObject;

import com.pointcliki.map.LayeredMap;
import com.pointcliki.map.MapManager;
import com.pointcliki.map.TileSet;

public class ReliefMap extends LayeredMap {

	public byte[][] fRelief;
	public byte[][] fWalls;
	public Decal[][][] fDecals;
	
	public ReliefMap(MapManager m) {
		super(m);
	}

	@Override
	public void initWithJSON(JSONObject obj) {
		super.initWithJSON(obj);
		
		// Load the terrain
		fRelief = new byte[fWidth][fHeight];
		fWalls = new byte[fWidth][fHeight];
		
		String[] starr1 = obj.optString("relief").split(" ");
		String[] starr2 = obj.optString("walls").split(" ");
		for (int x = 0; x < fWidth; x++) {
			for (int y = 0; y < fHeight; y++) {
				fRelief[x][y] = (byte) (Integer.parseInt(starr1[x + y * fWidth]) - 128);
				fWalls[x][y] = (byte) (Integer.parseInt(starr2[x + y * fWidth]) - 128);
			}
		}
		
		// Calculate decals
		fDecals = new Decal[fWidth][fHeight][0];
		for (int x = 0; x < fWidth; x++) {
			for (int y = 0; y < fHeight; y++) {
				fDecals[x][y] = calculateDecals(x, y);
			}
		}
	}
	
	public Decal[] decals(int x, int y) {
		return fDecals[Math.max(0, Math.min(x, fWidth - 1))][Math.max(0, Math.min(y, fHeight - 1))];
	}
	
	protected TileSet createTileSet(String source) {
		return new ReliefTileSet(source);
	}
	
	public int relief(int x, int y) {
		return fRelief[Math.max(0, Math.min(x, fWidth - 1))][Math.max(0, Math.min(y, fHeight - 1))] + 128;
	}

	public int wall(int x, int y) {
		return fWalls[Math.max(0, Math.min(x, fWidth - 1))][Math.max(0, Math.min(y, fHeight - 1))] + 128;
	}
	
	public Decal[] calculateDecals(int x, int y) {
		return new Decal[0];
	}
}
