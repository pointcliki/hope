package com.pointcliki.map.relief;

public enum TileMode {
	Tile, Wall, Decal, Shadow
}
