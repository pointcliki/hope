package com.pointcliki.map.relief;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newdawn.slick.geom.Rectangle;

import com.pointcliki.map.TileSet;

public class ReliefTileSet extends TileSet {
	
	protected int fTileRelief;
	protected HashMap<String, ArrayList<Rectangle>> fTiles;
	protected ArrayList<ArrayList<Rectangle>> fWallPrefs;

	public ReliefTileSet(String source) {
		super(source);
		fTiles = new HashMap<String, ArrayList<Rectangle>>();
		fWallPrefs = new ArrayList<ArrayList<Rectangle>>();
	}
	
	public void draw(int x, int y, int tileIndex, int wallIndex, int tileMinHeight, int tileMaxHeight, Decal[] decals) {
		drawWall(x, y, wallIndex, tileMinHeight, tileMaxHeight);
		drawTile(x, y, tileIndex, tileMaxHeight);
		drawDecals(x, y, tileMaxHeight, decals);
	}
	
	public void drawTile(int x, int y, int tileIndex, int tileHeight) {
		Rectangle r = fTiles.get("floors").get(tileIndex);
		fImage.drawEmbedded(x * fTileWidth, y * fTileHeight - tileHeight * fTileRelief, x * fTileWidth + r.getWidth(), y * fTileHeight - tileHeight * fTileRelief + r.getHeight(), r.getX(), r.getY(), r.getWidth() + r.getX(), r.getY() + r.getHeight());
	}
	
	private void drawDecals(int x, int y, int tileHeight, Decal[] decals) {
		for (Decal d: decals) {
			Rectangle r = tileRects("decals", d.type);
			fImage.drawEmbedded(x * fTileWidth + d.offset.x, y * fTileHeight - tileHeight * fTileRelief + d.offset.y, x * fTileWidth + r.getWidth() + d.offset.x, y * fTileHeight - tileHeight * fTileRelief + d.offset.y + r.getHeight(), r.getX(), r.getY(), r.getWidth() + r.getX(), r.getY() + r.getHeight());
		}
	}
	
	public Rectangle tileRects(String mode, String type) {
		return fTiles.get(mode).get(fTypeCache.get(mode + "." + type).get(0));
	}

	public void drawWall(int x, int y, int wallIndex, int tileMinHeight, int tileMaxHeight) {
		ArrayList<Rectangle> prefs = fWallPrefs.get(wallIndex);
		int i = 0;
		int h = tileMinHeight * fTileRelief;
		Rectangle r = prefs.get(0);
		int pixHeight = tileMaxHeight * fTileRelief;
		try {
			while (h < pixHeight) {
				while (r.getHeight() > pixHeight - h) {
					i++;
					r = prefs.get(i);
					if (r == null) {
						i++;
						r = prefs.get(i);
					}
				}
				fImage.drawEmbedded(x * fTileWidth, (y + 1) * fTileHeight - h - r.getHeight(), x * fTileWidth + r.getWidth(), (y + 1) * fTileHeight - h, r.getX(), r.getY(), r.getWidth() + r.getX(), r.getY() + r.getHeight());
				h += r.getHeight();
				// Check for plus
				if (prefs.size() > i + 2 && prefs.get(i + 1) != null) {
					i++;
					r = prefs.get(i);
				}
			}
		} catch (Exception e) {
			// Silent as in render loop
		}
	}
	
	@Override
	protected void initWithJSON(JSONObject config) {
		
		fConfig = config;
		
		try {
			JSONArray xyz = config.getJSONArray("xyz");
			fTileWidth = xyz.getInt(0);
			fTileHeight = xyz.getInt(1);
			fTileRelief = xyz.getInt(2);
		} catch (Exception e) {
			System.err.println("Couldn't read tile dimensions - defaulting to 32 x 32 x 32");
		}
		// Get regions
		JSONArray regions;
		try {
			regions = config.getJSONArray("regions");
		} catch (Exception e) {
			System.err.println("Couldn't find the regions object in the tileset");
			return;
		}
		// Iterate through the regions
		for (int i = 0; i < regions.length(); i++) {
			try {
				JSONObject obj = regions.getJSONObject(i);
				String mode = obj.getString("mode");
				JSONArray area = obj.getJSONArray("area");
				JSONArray xy = obj.getJSONArray("xy");
				JSONArray types = obj.getJSONArray("types");
				int tx = area.getInt(0);
				int ty = area.getInt(1);
				int tw = xy.getInt(0);
				int th = xy.getInt(1);
				int rw = area.getInt(2) / tw;
				// Create array
				if (!fTiles.containsKey(mode)) fTiles.put(mode, new ArrayList<Rectangle>());
				// Iterate through tiles
				for (int j = 0; j < types.length(); j++) {
					String s = types.optString(j);
					
						// Add to tiles
						fTiles.get(mode).add(new Rectangle(tx + (j % rw) * tw, ty + (j / rw) * th, tw, th));
						// Put in cache
						if (!fTypeCache.containsKey(mode + "." + s)) fTypeCache.put(mode + "." + s, new ArrayList<Integer>());
						fTypeCache.get(mode + "." + s).add(fTiles.get(mode).size() - 1);
				}
			} catch (Exception e) {
				System.err.println("Couldn't parse region " + i);
				System.err.println(e.getMessage());
			}
		}
		// Get wall preferences
		try {
			JSONArray prefs = config.getJSONArray("wallprefs");
			for (int i = 0; i < prefs.length(); i++) {
				ArrayList<Rectangle> list = new ArrayList<Rectangle>();
				String[] ss = prefs.getString(i).split(" ");
				for (String s: ss) {
					if (s.equals("+")) list.add(null);
					else list.add(tileRects("walls", s));
				}
				fWallPrefs.add(list);
			}
		} catch (Exception e) {
			System.err.println("Couldn't parse wall preferences");
			System.err.println(e.getMessage());
		}
	}
}

