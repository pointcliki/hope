package com.pointcliki.map;

public class MapString {
	public static <T> String ArrayXYToString(T[][] array, int minColumnWidth) {
		
		if (array.length == 0) return null; 
				
		String s = "";
		
		for (int y = 0; y < array[0].length; y++) {
			for (int x = 0; x < array.length; x++) {
				String text;
				if (array[x][y] != null) {
					text = array[x][y].toString();
				} else {
					text = "null";
				}
				while (text.length() < minColumnWidth) text = " " + text;
				
				s += text + " ";
			}
			s += "\n";
		}
		
		return s;
	}	
}
