package com.pointcliki.map;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class LayeredMap extends Map {
	
	private ArrayList<TileSet> fTilesets;
	public int fWidth = 16;
	public int fHeight = 16;
	public byte[][][] fTiles;
	
	public LayeredMap(MapManager m) {
		super(m);
		fTilesets = new ArrayList<TileSet>();
	}
	
	protected void addTileSet(TileSet s) {
		fTilesets.add(s);
	}
	
	@Override
	public void initWithJSON(JSONObject obj) {
		super.initWithJSON(obj);
		try {
			JSONArray xy = obj.optJSONArray("xy");
			fWidth = xy.optInt(0);
			fHeight = xy.optInt(1);
		} catch (Exception e) {
			System.err.println("Couldn't read level dimensions - defaulting to 16 x 16");
		}
		try {
			JSONArray sets = obj.optJSONArray("tilesets");
			for (int i = 0; i < sets.length(); i++) {
				TileSet set = createTileSet(sets.getString(i));
				fTilesets.add(set);
				set.init();
			}			
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Couldn't read level tilesets");
		}
		try {
			fTiles = new byte[1][fWidth][fHeight];
			String[] starr2 = obj.optString("tiles").split(" ");
			for (int x = 0; x < fWidth; x++) {
				for (int y = 0; y < fHeight; y++) {
					fTiles[0][x][y] = (byte) (Integer.parseInt(starr2[x + y * fWidth]) - 128);
				}
			}			
		} catch (Exception e) {
			System.err.println("Couldn't read level tiles");
		}
		
	}
	
	protected TileSet createTileSet(String source) {
		return new TileSet(source);
	}

	
	public TileSet tileset(int index) {
		return fTilesets.get(index);
	}
	
	@Override
	public int tile(int x, int y, int layer) {
		return fTiles[layer][Math.max(0, Math.min(x, fWidth - 1))][Math.max(0, Math.min(y, fHeight - 1))] + 128;
	}
	
	@Override
	public void tile(int x, int y, int layer, int tile) {
		fTiles[layer][Math.max(0, Math.min(x, fWidth - 1))][Math.max(0, Math.min(y, fHeight - 1))] = (byte) (tile - 128);
	}
	
	@Override
	public int width() {
		return fWidth;
	}
	
	@Override
	public int height() {
		return fHeight;
	}
}
