package com.pointcliki.map;

/**
	@since 3
*/

public interface IMapObject {
	public void init(Map map);
	public Map map();
}
