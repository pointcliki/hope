package com.pointcliki.map;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newdawn.slick.Image;

import com.pointcliki.core.PointClikiGame;

public class TileSet {

	protected String fSource;
	protected Image fImage;
	protected JSONObject fConfig;
	protected HashMap<String, ArrayList<Integer>> fTypeCache;
	protected int fTileWidth = 32;
	protected int fTileHeight = 32;
	
	public TileSet(String source) {
		fSource = source;
		fTypeCache = new HashMap<String, ArrayList<Integer>>();
		fImage = loadImage();
	}
	
	public void init() {
		initWithJSON(loadConfig());
	}
	
	protected Image loadImage() {
		return PointClikiGame.resourceManager().image("../tilesets/" + fSource);
	}
	
	protected JSONObject loadConfig() {
		return PointClikiGame.resourceManager().json("tilesets/" + fSource);
	}
	
	protected void initWithJSON(JSONObject obj) {
		fConfig = obj;
		try {
			JSONArray xy = obj.optJSONArray("xy");
			fTileWidth = xy.optInt(0);
			fTileHeight = xy.optInt(1);
		} catch (Exception e) {
			System.err.println("Couldn't read tile dimensions - defaulting to 32 x 32");
		}
		JSONArray arr = obj.optJSONArray("types");
		if (arr != null) {
			for (int i = 0; i < arr.length(); i++) {
				String s = arr.optString(i);
				if (!fTypeCache.containsKey(s)) fTypeCache.put(s, new ArrayList<Integer>());
				fTypeCache.get(s).add(i);
			}
		}
	}
	
	public String source() {
		return fSource;
	}
	
	public void startUse() {
		fImage.startUse();
	}
	
	public void endUse() {
		fImage.endUse();
	}
	
	public String type(int tileIndex) {
		JSONArray arr = fConfig.optJSONArray("types");
		if (arr == null) return null;
		return arr.optString(tileIndex);
	}
	
	public void draw(int x, int y, int tileIndex) {
		int w = fImage.getWidth() / fTileWidth;
		int tx = tileIndex % w;
		int ty = tileIndex / w;
		fImage.drawEmbedded(x * fTileWidth, y * fTileHeight, (x + 1) * fTileWidth, (y + 1) * fTileHeight, tx * fTileWidth, ty * fTileHeight, (tx + 1) * fTileWidth, (ty + 1) * fTileHeight);
	}
	
	public void draw(int x, int y, String type) {
		draw(x, y, type, 0);
	}
	
	public void draw(int x, int y, String type, int alternate) {
		if (fTypeCache.containsKey(type)) {
			ArrayList<Integer> arr = fTypeCache.get(type);
			if (arr.size() > alternate) {
				int i = arr.get(alternate);
				draw(x, y, i);
			}
		}
	}
}
