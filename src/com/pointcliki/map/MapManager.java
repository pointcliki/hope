package com.pointcliki.map;

import org.json.JSONObject;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

import com.pointcliki.core.IManagerGroup;
import com.pointcliki.core.Manager;
import com.pointcliki.core.PointClikiGame;

public abstract class MapManager extends Manager {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 6942950923054216831L;
	
	private Map fMap;
	
	public MapManager(IManagerGroup parent) {
		super(parent);
	}

	@Override
	public void restore(Manager from) {
		// TODO Auto-generated method stub

	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub

	}
	
	public Map map() {
		return fMap;
	}

	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		return (Manager) super.clone();
	}

	public void load(String string) {
		// Unload the old map
		if (fMap != null) fMap.cleanup();
		// Load the new map
		if (string.endsWith(".json")) {
			fMap = createMapFromJSON(PointClikiGame.resourceManager().json(string.substring(0, string.length() - 5)));
			
		} else if (string.endsWith(".tmx")) {
			try {
				fMap = createMapFromTMX(new TiledMap(string));
			} catch (SlickException e) {
				System.err.println("An error occurred trying to load the TiledMap " + string);
				System.err.println(e.getMessage());
			}
		}
	}
	
	public void save(String string) {
		
	}
	
	public void saveAs(String string) {
		
		
	}
	
	protected abstract Map createMapFromJSON(JSONObject obj);
	
	protected abstract Map createMapFromTMX(TiledMap map);

	public abstract IMapFactory entityFactory();
}
