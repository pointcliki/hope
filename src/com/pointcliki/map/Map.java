package com.pointcliki.map;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class Map {
	private MapManager fMapManager;
	private MapDispatcher fDispatcher;
	private ArrayList<IMapObject> fActives;

	public Map(MapManager m) {
		fMapManager = m;
		fDispatcher = new MapDispatcher();
		fActives = new ArrayList<IMapObject>();
	}
	
	public MapManager mapManager() {
		return fMapManager;
	}
	
	public void initWithJSON(JSONObject obj) {
		JSONArray actives = obj.optJSONArray("actives");
		for (int i = 0; i < actives.length(); i++) {
			IMapObject e = fMapManager.entityFactory().createFromJSON(actives.optJSONObject(i));
			addActive(e);
		}
	}
	
	public void cleanup() {
		// TODO: Dispatch tear down event
		fMapManager = null;
		fDispatcher = null;
	}
	
	public MapDispatcher dispatcher() {
		return fDispatcher;
	}
	
	public abstract int width();
	public abstract int height();
	public abstract int tile(int x, int y, int layer);

	public abstract void tile(int x, int y, int layer, int tile);
	
	@SuppressWarnings("unchecked")
	public List<IMapObject> actives() {
		return (List<IMapObject>) fActives.clone();
	}
	
	public void addActive(IMapObject e) {
		fActives.add(e);
		fDispatcher.dispatchEvent(MapEvent.ACTIVE_ADDED, e);
	}
	
	public void updateActive(IMapObject e) {
		fDispatcher.dispatchEvent(MapEvent.ACTIVE_UPDATED, e);
	}
	
	public void removeActive(IMapObject e) {
		fActives.remove(e);
		fDispatcher.dispatchEvent(MapEvent.ACTIVE_REMOVED, e);
	}
}
