package com.pointcliki.map;

import com.pointcliki.event.Dispatcher;

public class MapDispatcher extends Dispatcher<MapEvent> {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -3600522466248234623L;

	protected void dispatchEvent(String type, IMapObject e) {
		dispatchEvent(type, new MapEvent(e));
	}
}
