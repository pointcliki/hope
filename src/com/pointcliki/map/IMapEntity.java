package com.pointcliki.map;

/**
 * An entity that is aware of a map manager in the scene
 *  
 * @author hugheth
 * @since 3
 */
public interface IMapEntity {

	public MapManager mapManager();
}
