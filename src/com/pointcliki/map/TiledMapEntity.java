package com.pointcliki.map;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.tiled.TiledMap;

import com.pointcliki.core.Entity;
import com.pointcliki.core.ISavable;
import com.pointcliki.ui.UIEntity;

public abstract class TiledMapEntity extends UIEntity {

	/**
	 * Serial Key
	 */
	private static final long serialVersionUID = 8175683365734838012L;
	
	private TiledMap fMap = null;
	private ArrayList<ITiledMapObject> fObjects;
	
	public TiledMapEntity(TiledMapMap map) {
		fMap = map.tiledMap();
		fPosition = new Vector2f(0, 0);
		fSpan = new Rectangle(0, 0, fMap.getWidth() * fMap.getTileWidth(), fMap.getHeight() * fMap.getTileHeight());
	}

	@Override
	public ISavable snapshot() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}
	
	public void init() {
		fObjects = new ArrayList<ITiledMapObject>();
		int g = fMap.getObjectGroupCount();
		for (int i = 0; i < g; i++) {
			int c = fMap.getObjectCount(i);
			for (int j = 0; j < c; j++) {
				ITiledMapObject obj = createObjectFromTMX(new TMXObjectInfo(i, j));
				fObjects.add(obj);
				addChild((Entity) obj);
				obj.init(null);
			}
		}
	}
	
	protected abstract ITiledMapObject createObjectFromTMX(TMXObjectInfo info);

	@Override
	public void render(Graphics graphics, long currentTime) {
		if (fMap == null) return;
		
		// Render the map
		fMap.render((int) fPosition.getX(), (int) fPosition.getY());
	}
	
	public TiledMap tiledMap() {
		return fMap;
	}
	
	public class TMXObjectInfo {
		
		private int fGroup;
		private int fIndex;
		
		protected TMXObjectInfo(int group, int index) {
			fGroup = group;
			fIndex = index;			
		}
		
		public String name() {
			return tiledMap().getObjectName(fGroup, fIndex);
		}
		public String type() {
			return tiledMap().getObjectType(fGroup, fIndex);
		}
		public String property(String name, String def) {
			return tiledMap().getObjectProperty(fGroup, fIndex, name, def);
		}
		public Vector2f position() {
			return new Vector2f(tiledMap().getObjectX(fGroup, fIndex), tiledMap().getObjectY(fGroup, fIndex));
		}
		public Vector2f size() {
			return new Vector2f(tiledMap().getObjectWidth(fGroup, fIndex), tiledMap().getObjectHeight(fGroup, fIndex));
		}
	}
	
	@Override
	public String toString() {
		return "[TiledMapEntity:" + super.toString() + "]";
	}
}
