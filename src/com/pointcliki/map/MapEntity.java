package com.pointcliki.map;

import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.ui.UIEntity;

/**
 * An entity that renders a map in the scene
 *  
 * @author hugheth
 * @since 3
 */
public abstract class MapEntity extends UIEntity {
	
	private Map fMap;
	private Vector2f fOffset;

	public MapEntity(Map m) {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -2640568788423587053L;

	public Map map() {
		return fMap;
	}
	
	public Vector2f offset() {
		return fOffset;
	}
	
	public abstract void init();
}
