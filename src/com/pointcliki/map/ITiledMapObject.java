package com.pointcliki.map;

import com.pointcliki.map.TiledMapEntity.TMXObjectInfo;

public interface ITiledMapObject extends IMapObject {

	public void importFromTMX(TMXObjectInfo info);
}
