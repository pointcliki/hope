package com.pointcliki.transition;

import com.pointcliki.core.ISavable;

/**
 * The lerper class is used to perform a smooth transition between one value
 * and another of varying behaviour.
 * 
 * @author Hugheth
 * @since 2
 */
public abstract class Lerper implements ISavable {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 3146841409361397712L;
	
	protected long fStartTime;
	protected long fDuration;
	
	/**
	 * Setup the lerper so it is ready to be used
	 * @param startTime The starting time of the lerper
	 * @param fTimeDuration The duration of the lerp period
	 */
	public void setup(long startTime, long fTimeDuration) {
		fStartTime = startTime;
		fDuration = fTimeDuration;
	}

	/**
	 * @param currentTime The current time to retrieve an interval for
	 * @return The current interval
	 */
	public abstract float interval(long currentTime);
	
	/**
	 * @return The length of the lerp
	 */
	public long timeDuration() {
		return fDuration;
	}

	@Override
	public ISavable snapshot() throws CloneNotSupportedException {
		return (ISavable) super.clone();
	}
}
