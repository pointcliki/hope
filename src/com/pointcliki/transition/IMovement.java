package com.pointcliki.transition;

import com.pointcliki.core.Entity;

/**
 * Classes implementing this interface indicate that they change an entity's
 * position.
 * 
 * @author Hugheth
 * @since 3
 */
public interface IMovement {
	
	/**
	 * @return The entity that is being moved
	 */
	public Entity entity();
	
	/**
	 * @return Whether this movement is actively running on the entity
	 */
	public boolean isRunning();
}
