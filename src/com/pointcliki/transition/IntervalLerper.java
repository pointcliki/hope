package com.pointcliki.transition;

/**
 * A lerper which interpolates.
 * 
 * @author Hugheth
 * @since 3
 */

public class IntervalLerper extends Lerper {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -7109999629545611651L;

	@Override
	public float interval(long currentTime) {
		return (currentTime - fStartTime + 0.0f) / fDuration;
	}

}
