package com.pointcliki.transition;

/**
 * A lerper which interpolates between the start time and end time using an
 * easing function.
 * 
 * @author Hugheth
 * @since 3
 */

public class EasingLerper extends Lerper {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -7109999629545611651L;
	
	/**
	 * Start slowly and accelerate
	 */
	public static final int EASE_IN = 0;
	/**
	 * Start fast and grind to a halt
	 */
	public static final int EASE_OUT = 1;

	protected int fType;
	
	/**
	 * Create a new lerper
	 * @param type The type of easing function
	 */
	public EasingLerper(int type) {
		fType = type;
	}

	@Override
	public float interval(long currentTime) {
		float iv = (currentTime - fStartTime + 0.0f) / fDuration;
		if (fType == EASE_IN) return iv * iv * iv;
		else if (fType == EASE_OUT) return (float) (1 - Math.pow(1 - iv, 3));
		
		return iv;
	}

}
