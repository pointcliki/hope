package com.pointcliki.transition;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.Entity;
import com.pointcliki.core.ISavable;
import com.pointcliki.event.Minion;
import com.pointcliki.event.ProgressEvent;

/**
 * A lerp movement uses a lerper to move an entity
 * @author hugheth
 * @since 3
 * 
 * @param <T> The type of entity that the movement functions on
 */
public class LerpMovement <T extends Entity> extends Transition<T> implements ISavable, IMovement {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 785978939281701297L;
	
	protected Vector2f fTarget = null;
	protected Vector2f fOrigin;
	protected long fTimeDuration;

	/**
	 * @param entity The entity the movement will move
	 * @param monitor A minion which monitors the movement's progress
	 * @param renderPriority The priority of render
	 * @param logicPriority The priority of transition end
	 */
	public LerpMovement(T entity, Minion<ProgressEvent<T>> monitor, int renderPriority, int logicPriority) {
		super(entity, monitor, renderPriority, logicPriority);
	}
	
	/**
	 * @param lerper The lerper used by the transition
	 * @param unitTimeDuration The length of time to move a single grid square
	 */
	@Override
	public void setup(long unitTimeDuration, Lerper lerper) {
		fLerper = lerper;
		fTimeDuration = unitTimeDuration;
	}
	
	
	/**
	 * Set up a new lerp movement
	 * @param target The target of the movement
	 */
	public void setup(Vector2f target) {
		assert !isRunning(): "Can't setup a movement if it's already running!";
		fOrigin = fParent.position();
		fTarget = target;
		super.setup(fTimeDuration, fLerper);
	}
	
	@Override
	public void begin() {
		assert fTarget != null: "Can't begin a movement if it hasn't been setup!";
		super.begin();
	}
	
	@Override
	public void end() {
		// Move to the target
		fParent.position(fTarget);
		fTarget = null;
		super.end();
	}
	
	@Override
	public void cancel() {
		super.cancel();
		// Move back to the origin
		fParent.position(fOrigin);
	}
	
	@Override
	public void pause() {
		super.pause();
	}	

	@Override
	public ISavable snapshot() throws CloneNotSupportedException {
		@SuppressWarnings("unchecked")
		LerpMovement<T> clone = (LerpMovement<T>) super.clone();
		clone.fTarget = fTarget.copy();
		clone.fOrigin = fOrigin.copy();
		return clone;
	}

	@Override
	protected void run(Graphics graphics, long currentTime, float interval) {
		// Re-use position variable to avoid creating a new object during render phase
		Vector2f pos = fParent.position();
		pos.set(fOrigin);
		pos.x += (fTarget.x - pos.x) * interval;
		pos.y += (fTarget.y - pos.y) * interval;
		fParent.position(pos);
	}
}
