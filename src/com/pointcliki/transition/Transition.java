package com.pointcliki.transition;

import org.newdawn.slick.Graphics;

import com.pointcliki.core.Entity;
import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.FrameEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.event.ProgressEvent;
import com.pointcliki.event.RenderRogue;
import com.pointcliki.event.Rogue;

/**
 * The transition class is a form of rogue that has a known duration, and is
 * attached to an entity, modifying some of its properties through a certain
 * time period using a lerper.
 * 
 * @author Hugheth
 * @since 3
 * 
 * @see Rogue
 * @see Entity
 * 
 * @param <T> The type of entity the transition works with 
 */
public abstract class Transition<T extends Entity> extends RenderRogue<T> {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 9118367319693339236L;
	
	protected Lerper fLerper; 
	protected long fFrameDuration = -1;
	protected EndTransition fEndTransition;
	protected long fEndFrame;

	/**
	 * Create a new transition
	 * @param entity The entity the transition is parented to
	 * @param monitor The monitor that tracks the transition's progress
	 * @param renderPriority The priority to render the transition with
	 * @param logicPriority The logical priority to end the transition with
	 */
	public Transition(T entity, Minion<ProgressEvent<T>> monitor, int renderPriority, int logicPriority) {
		super(entity, monitor, renderPriority);
		fEndTransition = new EndTransition(logicPriority);
	}
	
	/**
	 * Set up the lerper and specify the time duration of the transition
	 * @param timeDuration
	 * @param lerper
	 */
	public void setup(long timeDuration, Lerper lerper) {
		// Use an under-estimation 
		fFrameDuration = (int) Math.floor(fParent.timeManager().frameDuration(timeDuration));
		fLerper = lerper;
		lerper.setup(fParent.timeManager().currentTime(), timeDuration);
	}
	
	@Override
	public void begin() {
		assert fFrameDuration >= 0: "Transition hasn't been setup";
		super.begin();
		// Add the end minion
		fEndFrame = fFrameDuration + fParent.timeManager().currentFrame();
		fParent.frameManager().queue(fEndTransition, fFrameDuration);
	}
	
	@Override
	protected void end() {
		// Set frame duration to -1 so we know we haven't setup the next transition yet
		fFrameDuration = -1;
		super.end();
	}
	
	@Override
	protected void run(Graphics graphics, long currentTime) {
		float interval = Math.min(1, Math.max(0, fLerper.interval(currentTime)));
		run(graphics, currentTime, interval);
	}
	
	@Override
	public void cancel() {
		super.cancel();
		fParent.frameManager().dequeue(fEndTransition, fEndFrame);
	}
	
	public void pause() {
		super.pause();
		fParent.frameManager().dequeue(fEndTransition, fEndFrame);
	}
	
	/**
	 * Extending classes should override this method to perform the expected
	 * operation on 
	 * 
	 * @param graphics
	 * @param view
	 * @param currentTime
	 * @param interval The lerp interval of the current transition
	 */
	protected abstract void run(Graphics graphics, long currentTime, float interval);
	
	protected class EndTransition extends Minion<FrameEvent> {
		public EndTransition(int priority) {
			super(priority);
		}

		@Override
		public long run(Dispatcher<FrameEvent> dispatcher, String type, FrameEvent event) {
			if (fRunning) end();
			return FINISH;
		}
	}
	
	@Override
	public Transition<T> snapshot(T entity) throws CloneNotSupportedException {
		@SuppressWarnings("unchecked")
		Transition<T> clone = (Transition<T>) super.snapshot(entity);
		clone.fLerper = (Lerper) fLerper.snapshot();
		return clone;
	}
}
