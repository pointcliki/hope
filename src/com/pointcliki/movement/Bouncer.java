package com.pointcliki.movement;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.Entity;
import com.pointcliki.event.RenderRogue;

public class Bouncer<T extends Entity> extends RenderRogue<T> {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 712788521769219920L;
	
	private float fTime;
	private float fHeight;
	private long fStartTime;

	public Bouncer(T entity, int renderPriority, float time, float height) {
		super(entity, null, renderPriority);
		fTime = (float) (time / Math.PI);
		fHeight = height;
	}
	
	@Override
	public void begin() {
		super.begin();
		fStartTime = entity().timeManager().currentTime();
	}

	@Override
	protected void run(Graphics graphics, long currentTime) {
		entity().position(new Vector2f(entity().position().x, entity().position().y - (float) (Math.abs(Math.sin((currentTime - fStartTime) / fTime)) * fHeight)));
	}
}

