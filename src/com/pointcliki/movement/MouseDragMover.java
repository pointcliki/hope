package com.pointcliki.movement;

import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.IEvent;
import com.pointcliki.event.Minion;
import com.pointcliki.event.ProgressEvent;
import com.pointcliki.event.Rogue;
import com.pointcliki.grid.GridCoordinate;
import com.pointcliki.input.MouseEvent;
import com.pointcliki.ui.UIEntity;

/**
 * This rogue is used to drag an entity 
 * 
 * @author hugheth
 * @since 3
 * @param <T> The type of entity
 */

public class MouseDragMover<T extends UIEntity> extends Rogue<T> {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 1985973131929771131L;
	
	private Rectangle fBox;
	private Minion<IEvent> fMinion;
	private Vector2f fXY;

	public MouseDragMover(T entity, Minion<ProgressEvent<T>> monitor) {
		super(entity, monitor);
		fMinion = new Minion<IEvent>() {
			@Override
			public long run(Dispatcher<IEvent> dispatcher, String type, IEvent event) {
				if (event instanceof MouseEvent) {
					MouseEvent m = (MouseEvent) event;
					
					fXY = new Vector2f(m.x(), m.y());
					check();
				}
				return Minion.CONTINUE;
			}
		};
	}
	
	public void check() {
		if (!entity().capturing() || isPaused()) return;
		
		int x = (int) (fXY.x - entity().position().x);
		int y = (int) (fXY.y - entity().position().y);
		
		if (x > fBox.getMaxX()) {
			fMonitor.run(null, "drag", new MouseDragMoverEvent<T>(MouseDragMover.this, GridCoordinate.EAST));
		} else if (x  < fBox.getX()) {
			fMonitor.run(null, "drag", new MouseDragMoverEvent<T>(MouseDragMover.this, GridCoordinate.WEST));
		} else if (y > fBox.getMaxY()) {
			fMonitor.run(null, "drag", new MouseDragMoverEvent<T>(MouseDragMover.this, GridCoordinate.SOUTH));
		} else if (y < fBox.getY()) {
			fMonitor.run(null, "drag", new MouseDragMoverEvent<T>(MouseDragMover.this, GridCoordinate.NORTH));
		}
	}
	
	public void setup(Rectangle rect) {
		fBox = rect;
	}
	
	@Override
	public void begin() {
		super.begin();
		entity().dispatcher().addMinion("mouse.drag", fMinion);
	}
	
	@Override
	public void cancel() {
		super.cancel();
		end();
	}
	
	@Override
	protected void end() {
		super.end();
		entity().dispatcher().removeMinion("mouse.drag", fMinion);
	}
	
	public static class MouseDragMoverEvent<S extends UIEntity> extends ProgressEvent<S> {
		
		private GridCoordinate fTile;

		public MouseDragMoverEvent(Rogue<S> rogue, GridCoordinate tile) {
			super(rogue);
			fTile = tile;			
		}
		
		public GridCoordinate tile() {
			return fTile;
		}
	}
}

