package com.pointcliki.movement;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.Entity;
import com.pointcliki.event.RenderRogue;

public class Wobbler<T extends Entity> extends RenderRogue<T> {
	
	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -4538140932379933464L;

	private float fTimeOffset;
	private float fTime;
	private float fWidth;
	private float fHeight;

	public Wobbler(T entity, int renderPriority, float time, float widthScale, float heightScale) {
		this(entity, renderPriority, time, widthScale, heightScale, (float) (Math.random() * 2 * Math.PI));
	}
	
	public Wobbler(T entity, int renderPriority, float time, float widthScale, float heightScale, float timeOffset) {
		super(entity, null, renderPriority);
		fTime = (float) (time / (2 * Math.PI));
		fWidth = widthScale;
		fHeight = heightScale;
		fTimeOffset = timeOffset;
	}
	
	@Override
	public void cancel() {
		super.cancel();
		entity().scale(new Vector2f(1f, 1f));
	}


	@Override
	protected void run(Graphics graphics, long currentTime) {
		entity().scale(new Vector2f((float) (1 - Math.sin(currentTime / fTime + fTimeOffset) * fWidth), (float) (1 + Math.sin(currentTime / fTime + fTimeOffset) * fHeight)));
	}
}
	

