package com.pointcliki.io;

import com.pointcliki.core.IManagerGroup;
import com.pointcliki.core.Manager;

public class SnapshotManager extends Manager {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = -850067718692444173L;

	public SnapshotManager(IManagerGroup parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void cleanup() {}

	/**
	 * This method is not used as manager is parented to the game
	 */
	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		return null;
	}
	
	/**
	 * This method is not used as manager is parented to the game
	 */
	@Override
	public void restore(Manager from) {}
}
