package com.pointcliki.io;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

import com.pointcliki.core.Manager;

public class ResourceManager extends Manager {
	/**
	 * Serial Key
	 */
	private static final long serialVersionUID = 1L;
	private static UnicodeFont sUIFont;
	private String fRootDir;
	private HashMap<String, Image> fImageCache;
	private HashMap<String, JSONObject> fJSONCache;
	
	public ResourceManager(String rootDir) {
		super(null);
		fRootDir = rootDir;
		fImageCache = new HashMap<String, Image>();
		fJSONCache = new HashMap<String, JSONObject>();
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
	}

	public InputStream getStream(String string) {
		try {
			FileInputStream stream = new FileInputStream(string);
			return stream;
		} catch (FileNotFoundException e) {
			System.err.println("Couldn't find resource: " + string);
			return null;
		}
	}
	
	public JSONObject json(String string) {
		return json(string, false);
	}
	
	public JSONObject json(String string, boolean ignoreCache) {
		
		JSONObject obj = null;
		string += ".json";
		
		if (ignoreCache || !fJSONCache.containsKey(string)) {
			try {
				obj = new JSONObject(new JSONTokener(new FileReader(new File(string))));
			} catch (FileNotFoundException e) {
				System.err.println("A problem ocurred trying to locate the json file: " + string);
				System.err.println(e.getMessage());
			} catch (JSONException e) {
				System.err.println("A problem ocurred trying to parse the json file: " + string);
				System.err.println(e.getMessage());
			}		
			fJSONCache.put(string, obj);
		} else {
			obj = fJSONCache.get(string);
		}
		return obj;
	}

	public Image image(String string) {
		if (fImageCache.containsKey(string)) return fImageCache.get(string);
		try {
			Image i = new Image(fRootDir + "/" + string + ".png");
			fImageCache.put(string, i);
			return i;
		} catch (Exception e) {
			System.err.println("Couldn't find resource: " + fRootDir + "/" + string + ".png");
			return null;
		}
	}

	public Animation animation(String string) {
		try {
			return new Animation(new SpriteSheet(fRootDir + "/" + string + ".png", 32, 32), 100);
		} catch (Exception e) {
			System.err.println("Couldn't find resource: " + string);
			return null;
		}
	}
	
	public SpriteSheet spritesheet(String string, int x, int y) {
		try {
			return new SpriteSheet(fRootDir + "/" + string + ".png", x, y);
		} catch (Exception e) {
			System.err.println("Couldn't find resource: " + string);
			return null;
		}
	}
	
	public UnicodeFont UIFont() {
		if (sUIFont != null) return sUIFont;
		try {
			sUIFont = createFont("Arial", 14);
		} catch (SlickException e) {
			System.err.println("Unable to load UI font");
			return null;
		}
		return sUIFont;
	}

	/**
	 * This method is not used as the manager is parented to the game, not scene
	 */
	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * This method is not used as the manager is parented to the game, not scene
	 */
	@Override
	public void restore(Manager from) {
		// TODO Auto-generated method stub
		
	}
	
	@SuppressWarnings("unchecked")
	public static UnicodeFont createFont(String name, int size) throws SlickException {
		UnicodeFont font = new UnicodeFont(new Font(name, 0, size));
		font.getEffects().add(new ColorEffect(Color.white));
		font.addAsciiGlyphs();
		font.loadGlyphs();
		return font;
	}
	
	@SuppressWarnings("unchecked")
	public static UnicodeFont createLocalFont(String file, int size) throws SlickException {
		UnicodeFont font = new UnicodeFont(file, size, false, false);
		font.getEffects().add(new ColorEffect(Color.white));
		font.addAsciiGlyphs();
		font.loadGlyphs();
		return font;
	}
}

