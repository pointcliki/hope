package com.pointcliki.grid;

import com.pointcliki.grid.GridManager;

/**
 * An entity that is aware of a grid manager in the scene
 *  
 * @author hugheth
 * @since 3
 */
public interface IGridEntity {

	public GridManager gridManager();
	
	public GridCoordinate getTile();
	
	public void setTile(GridCoordinate tile);
	
	public long id();
}
