package com.pointcliki.grid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.newdawn.slick.geom.Vector2f;

import com.pointcliki.core.IManagerGroup;
import com.pointcliki.core.Manager;
import com.pointcliki.event.Dispatcher;
import com.pointcliki.event.IEvent;
import com.pointcliki.event.Minion;

public class GridManager extends Manager {

	/**
	 * Serial key
	 */
	private static final long serialVersionUID = 7981326138816143886L;
	
	protected HashMap<GridCoordinate, ArrayList<IGridEntity>> fGrid;
	protected Vector2f fSize;
	protected Vector2f fOrigin;
	protected GridDispatcher fDispatcher;

	public GridManager(IManagerGroup parent, Vector2f size, Vector2f origin) {
		super(parent);
		fGrid = new HashMap<GridCoordinate, ArrayList<IGridEntity>>();
		fSize = size;
		fOrigin = origin;
		fDispatcher = new GridDispatcher();
	}
	
	@SuppressWarnings("unchecked")
	public List<IGridEntity> getEntitiesAt(GridCoordinate coord) {
		ArrayList<IGridEntity> list = fGrid.get(coord);
		if (list != null) {
			return (List<IGridEntity>) list.clone();
		} else {
			return new ArrayList<IGridEntity>();
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> getEntitiesOfTypeAt(GridCoordinate coord, Class<T> cls) {
		List<IGridEntity> list = fGrid.get(coord);
		ArrayList<T> array = new ArrayList<T>();
		if (list != null) {
			for (IGridEntity e: list) if (cls.isInstance(e)) array.add((T) e);
			return array;
		} else {
			return new ArrayList<T>();
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getFirstEntityOfTypeAt(GridCoordinate coord, Class<T> cls) {
		List<IGridEntity> list = getEntitiesAt(coord);
		for (IGridEntity obj: list) {
			if (cls.isInstance(obj)) return (T) obj;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getFirstEntityOfTypeAt(GridCoordinate coord, Class<T> cls, T ignore) {
		List<IGridEntity> list = getEntitiesAt(coord);
		for (IGridEntity obj: list) {
			if (obj.getClass().equals(cls) && !obj.equals(ignore)) return (T) obj;
		}
		return null;
	}	
	
	public void addObject(GridCoordinate coord, IGridEntity obj) {
		if (!fGrid.containsKey(coord)) fGrid.put(coord, new ArrayList<IGridEntity>());
		fGrid.get(coord).add(obj);
	}
	
	public void removeObject(GridCoordinate coord, IGridEntity obj) {
		if (!fGrid.containsKey(coord)) return;
		fGrid.get(coord).remove(obj);
		if (fGrid.get(coord).size() == 0) fGrid.remove(coord);
	}
	
	public void removeAll(GridCoordinate coord) {
		if (fGrid.containsKey(coord)) fGrid.remove(coord);
	}
	public <T> void removeAll(GridCoordinate coord, Class<T> cls) {
		if (fGrid.containsKey(coord)) {
			@SuppressWarnings("unchecked")
			Iterator<T> it = (Iterator<T>) fGrid.get(coord).iterator();
			while (it.hasNext()) if (it.next().getClass().equals(cls)) it.remove();
		}
	}

	@Override
	public void restore(Manager from) {
		 
	}

	@Override
	public void cleanup() {
		fGrid = null;
	}

	@Override
	public Manager snapshot() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}

	public void moveObject(GridCoordinate prev, GridCoordinate next, IGridEntity entity) {
		removeObject(prev, entity);
		addObject(next, entity);
	}

	public Vector2f tileSize() {
		return fSize;
	}
	public Vector2f origin() {
		return fOrigin;
	}

	public Vector2f tileToVector(GridCoordinate tile) {
		return new Vector2f(tile.x() * fSize.x + fOrigin.x, tile.y() * fSize.y + fOrigin.y);
	}

	public GridCoordinate vectorToTile(Vector2f v) {
		return new GridCoordinate((int) ((v.x - fOrigin.x) / fSize.x), (int) ((v.y - fOrigin.y) / fSize.y));
	}
	
	public GridDispatcher dispatcher() {
		return fDispatcher;
	}
	
	public static class GridDispatcher extends Dispatcher<GridEvent> {
		/**
		 * Serial key
		 */
		private static final long serialVersionUID = -988139647884752863L;

		public void dispatch(GridCoordinate xy, String type, GridEvent e) {
			dispatchEvent(xy.toString() + "." + type, e);
		}
		
		public void addGridMinion(GridCoordinate xy, String type, Minion<GridEvent> minion) {
			addMinion(xy.toString() + "." + type, minion);
		}
		
		public void removeGridMinion(GridCoordinate xy, String type, Minion<GridEvent> minion) {
			removeMinion(xy.toString() + "." + type, minion);
		}
	}
	
	@Override
	public String toString() {
		return "[GridManager " + fGrid + "]";
	}
	
	public static class GridEvent implements IEvent {
		
	}
}